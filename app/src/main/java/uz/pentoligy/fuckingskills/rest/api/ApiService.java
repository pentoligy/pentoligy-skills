package uz.pentoligy.fuckingskills.rest.api;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import uz.pentoligy.fuckingskills.architect.model.search.SearchItem;
import uz.pentoligy.fuckingskills.architect.model.song.SongItem;

/**
 * Created by pentoligy on 4/23/17.
 */

public interface ApiService {

    @GET(ApiUrl.PREFIX_URL_SEARCH)
    Observable<SearchItem> getSearchItem(@Query(ApiUrl.PREFIX_QUERY) String searchQuery);

    @GET(ApiUrl.PREFIX_URL_SONG + "{path}")
    Observable<SongItem> getSongItem(@Path("path") Long id);
}
