package uz.pentoligy.fuckingskills.rest.interceptor;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;
import uz.pentoligy.fuckingskills.rest.api.ApiUrl;

/**
 * Created by pentoligy on 4/23/17.
 */

public class BearerInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        return chain.proceed(chain.request().newBuilder()
                .addHeader("Authorization", "Bearer " + ApiUrl.AUTH_TYPICAL_BEARER)
                .url(chain.request().url().newBuilder().addQueryParameter("text_format", "html").build())
                .build());
    }
}
