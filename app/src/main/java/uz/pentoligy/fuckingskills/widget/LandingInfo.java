package uz.pentoligy.fuckingskills.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.annotation.AttrRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import uz.pentoligy.fuckingskills.R;

/**
 * Created by pentoligy on 4/26/17.
 */

public class LandingInfo extends FrameLayout {
    private CardView cardView;
    private AppCompatImageView content;
    private AppCompatTextView text1, text2;

    private Drawable iconDrawable;
    private String titleString, textString;

    public LandingInfo(@NonNull Context context) {
        super(context);
        init(context, null);
    }

    public LandingInfo(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public LandingInfo(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.LandingInfo, 0, 0);
        iconDrawable = attributes.getDrawable(R.styleable.LandingInfo_landing_icon);
        titleString = attributes.getString(R.styleable.LandingInfo_landing_title);
        textString = attributes.getString(R.styleable.LandingInfo_landing_text);
        attributes.recycle();

        LayoutInflater.from(context).inflate((R.layout.utils_widget_landing_info), this, true);

        cardView = (CardView) findViewById(android.R.id.background);
        content = (AppCompatImageView) findViewById(android.R.id.content);
        text1 = (AppCompatTextView) findViewById(android.R.id.text1);
        text2 = (AppCompatTextView) findViewById(android.R.id.text2);

        cardView.setForeground(getSelectedItemDrawable());

        if (iconDrawable != null) {
            setIcon(iconDrawable);
        }

        if (titleString != null) {
            setTitle(titleString);
        }

        if (textString != null) {
            setText(textString);
        }
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener onClickListener) {
        super.setOnClickListener(onClickListener);
        cardView.setOnClickListener(onClickListener);
    }

    public void setIcon(@DrawableRes int drawableId) {
        iconDrawable = ContextCompat.getDrawable(getContext(), drawableId);
        content.setImageDrawable(iconDrawable);
    }

    public void setIcon(Drawable drawable) {
        iconDrawable = drawable;
        content.setImageDrawable(iconDrawable);
    }

    public void setTitle(@StringRes int stringId) {
        titleString = getResources().getString(stringId);
        text1.setText(titleString);
    }

    public void setTitle(String title) {
        titleString = title;
        text1.setText(titleString);
    }

    public void setText(@StringRes int stringId) {
        textString = getResources().getString(stringId);
        text2.setText(textString);
    }

    public void setText(String text) {
        textString = text;
        text2.setText(textString);
    }

    private Drawable getSelectedItemDrawable() {
        TypedArray typedArray = getContext().obtainStyledAttributes(new int[]{R.attr.selectableItemBackground});
        Drawable selectedItemDrawable = typedArray.getDrawable(0);
        typedArray.recycle();
        return selectedItemDrawable;
    }
}
