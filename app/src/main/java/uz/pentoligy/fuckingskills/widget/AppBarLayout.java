package uz.pentoligy.fuckingskills.widget;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.os.ParcelableCompat;
import android.support.v4.os.ParcelableCompatCreatorCallbacks;
import android.support.v4.view.AbsSavedState;
import android.util.AttributeSet;

/**
 * Created by pentoligy on 4/22/17.
 */

public class AppBarLayout extends android.support.design.widget.AppBarLayout {
    private boolean isExpanded;

    public AppBarLayout(Context context) {
        super(context);
    }

    public AppBarLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
        super.setExpanded(expanded, true);
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable parcelable = super.onSaveInstanceState();
        SavedState savedState = new SavedState(parcelable);
        savedState.isExpanded = isExpanded ? 1 : 0;
        return savedState;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        isExpanded = savedState.isExpanded == 1;
        setExpanded(isExpanded, false);
    }

    private static class SavedState extends AbsSavedState {
        private int isExpanded;

        SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel source, ClassLoader loader) {
            super(source, loader);
            isExpanded = source.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(isExpanded);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = ParcelableCompat.newCreator(new ParcelableCompatCreatorCallbacks<SavedState>() {

            @Override
            public SavedState createFromParcel(Parcel in, ClassLoader loader) {
                return new SavedState(in, loader);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }

        });
    }
}
