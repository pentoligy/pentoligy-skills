package uz.pentoligy.fuckingskills.core.ui.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.core.ui.dagger2.DaggerFragment;
import uz.pentoligy.fuckingskills.dagger2.component.FragmentComponent;
import uz.pentoligy.fuckingskills.dagger2.module.FragmentModule;
import uz.pentoligy.fuckingskills.ui.activity.MainActivity;
import uz.pentoligy.fuckingskills.widget.Toolbar;

/**
 * Created by pentoligy on 4/22/17.
 */

public abstract class BaseFragment extends DaggerFragment {
    private final static String INTERNAL_BUNDLE_ON_PAUSE_SAVED_INSTANCE_TAG = "internal_bundle_child_saved_instance";
    private FragmentComponent fragmentComponent;
    private MainActivity mainActivity;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mainActivity = (MainActivity) getActivity();

        fragmentComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusFragmentComponent(new FragmentModule(this));

        fragmentComponent.inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getFragmentLayout(), container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onViewCreated(view, savedInstanceState,
                getArguments() == null ?
                        null : getArguments().getBundle(INTERNAL_BUNDLE_ON_PAUSE_SAVED_INSTANCE_TAG) == null ?
                        null : getArguments().getBundle(INTERNAL_BUNDLE_ON_PAUSE_SAVED_INSTANCE_TAG));
    }

    @Override
    public void onPause() {
        if (getArguments() != null) {
            Bundle bundle = new Bundle();
            onPauseSavedInstanceState(bundle);
            getArguments().putBundle(INTERNAL_BUNDLE_ON_PAUSE_SAVED_INSTANCE_TAG, bundle);
        }

        super.onPause();
    }

    @Override
    public void onDestroyView() {
        if (getView() != null) {
            ((ViewGroup) getView()).removeAllViews();
        }
        super.onDestroyView();
    }

    @Override
    public void onDetach() {
        fragmentComponent = null;
        super.onDetach();
    }

    protected MainActivity getMainActivity() {
        return mainActivity;
    }

    protected Toolbar getToolbar() {
        return mainActivity.getToolbar();
    }

    public abstract int getFragmentLayout();

    public abstract void onViewCreated(View view, @Nullable Bundle parentInstanceState, @Nullable Bundle childInstanceState);

    public abstract void onPauseSavedInstanceState(Bundle outState);

}
