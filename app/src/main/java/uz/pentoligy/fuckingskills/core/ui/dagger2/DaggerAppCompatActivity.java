package uz.pentoligy.fuckingskills.core.ui.dagger2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import uz.pentoligy.SkillsApplication;

import uz.pentoligy.fuckingskills.dagger2.component.AppCompatActivityComponent;
import uz.pentoligy.fuckingskills.dagger2.module.AppCompatActivityModule;

/**
 * Created by pentoligy on 4/23/17.
 */

public abstract class DaggerAppCompatActivity extends AppCompatActivity {
    private AppCompatActivityComponent appCompatActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appCompatActivityComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusAppCompatActivityComponent(new AppCompatActivityModule(this));

        inject(appCompatActivityComponent);
    }

    @Override
    protected void onDestroy() {
        appCompatActivityComponent = null;
        super.onDestroy();
    }

    public abstract void inject(AppCompatActivityComponent appCompatActivityComponent);

}
