package uz.pentoligy.fuckingskills.core.ui.presenter;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.architect.presenter.GeneralPresenter;
import uz.pentoligy.fuckingskills.architect.view.BaseActivityView;
import uz.pentoligy.fuckingskills.core.ui.base.BaseAppCompatActivity;
import uz.pentoligy.fuckingskills.dagger2.component.AppCompatActivityComponent;
import uz.pentoligy.fuckingskills.dagger2.module.AppCompatActivityModule;

/**
 * Created by pentoligy on 4/23/17.
 */

public abstract class BaseActivityPresenterActivity<P extends GeneralPresenter<M>, M extends Parcelable>
        extends BaseAppCompatActivity implements BaseActivityView<M> {

    private P presenter;
    public abstract P setPresenter();

    private AppCompatActivityComponent appCompatActivityComponent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appCompatActivityComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusAppCompatActivityComponent(new AppCompatActivityModule(this));

        appCompatActivityComponent.inject(this);

        presenter = setPresenter();

        if (savedInstanceState != null) {
            presenter.restoreState(savedInstanceState);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        presenter.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        appCompatActivityComponent = null;

        if (presenter != null) {
            presenter.unbindView();
        }

        super.onDestroy();
    }

    public P getPresenter() {
        return presenter;
    }

    protected void bindView() {
        if (presenter.getView() == null && presenter != null) {
            presenter.bindView(this);
        }
    }
}
