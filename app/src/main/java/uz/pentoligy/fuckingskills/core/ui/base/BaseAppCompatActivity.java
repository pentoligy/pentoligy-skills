package uz.pentoligy.fuckingskills.core.ui.base;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.core.ui.dagger2.DaggerAppCompatActivity;
import uz.pentoligy.fuckingskills.dagger2.component.AppCompatActivityComponent;
import uz.pentoligy.fuckingskills.dagger2.module.AppCompatActivityModule;
import uz.pentoligy.fuckingskills.ui.activity.PopupActivity;

/**
 * Created by pentoligy on 4/22/17.
 */

public abstract class BaseAppCompatActivity extends DaggerAppCompatActivity {
    private AppCompatActivityComponent appCompatActivityComponent;
    private boolean isResumed;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        appCompatActivityComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusAppCompatActivityComponent(new AppCompatActivityModule(this));

        appCompatActivityComponent.inject(this);

        isResumed = true;
    }

    @Override
    protected void onResume() {
        isResumed = true;
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        appCompatActivityComponent = null;
        isResumed = false;
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        isResumed = false;
        super.onStop();
    }
    
    public Intent songInfoIntent(String[] songInfo) {
        return new Intent(this, PopupActivity.class)
                .putExtra(PopupActivity.NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY, songInfo);
    }

    public Intent shareUrlIntent(String musicInfo, String url) {
        return Intent.createChooser((new Intent()
                .setAction(Intent.ACTION_SEND)
                .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .putExtra(Intent.EXTRA_TEXT,
                        getString(R.string.share_find_out)
                                + musicInfo + getString(R.string.share_at_genius) + url)

                .setType("text/plain")), getString(R.string.share_title));
    }
}
