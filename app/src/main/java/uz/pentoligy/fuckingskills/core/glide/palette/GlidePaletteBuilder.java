package uz.pentoligy.fuckingskills.core.glide.palette;

import android.graphics.Bitmap;
import android.support.v7.graphics.Palette;
import android.widget.ImageView;

import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.ImageViewTarget;

/**
 * Created by pentoligy on 4/24/17.
 */

public class GlidePaletteBuilder extends ImageViewTarget<Bitmap> implements Palette.PaletteAsyncListener {
    private GlidePaletteAsyncListener glidePaletteAsyncListener;

    public GlidePaletteBuilder(GlidePaletteAsyncListener glidePaletteAsyncListener, ImageView view) {
        super(view);
        this.glidePaletteAsyncListener = glidePaletteAsyncListener;
    }

    @Override
    public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
        super.onResourceReady(resource, glideAnimation);
        Palette.from(resource).generate(this);
    }

    @Override
    protected void setResource(Bitmap resource) {
        view.setImageBitmap(resource);
    }

    @Override
    public void onGenerated(Palette palette) {
        if (glidePaletteAsyncListener != null) {
            glidePaletteAsyncListener.onGenerated(getTitleTextColor(palette), getBodyTextColor(palette));
        }
    }

    private int getTitleTextColor(Palette palette) {
        if (palette.getDominantSwatch() != null && palette.getDominantSwatch().getTitleTextColor() != 0) {
            return palette.getDominantSwatch().getTitleTextColor();
        } else if (palette.getMutedSwatch() != null && palette.getMutedSwatch().getTitleTextColor() != 0) {
            return palette.getMutedSwatch().getTitleTextColor();
        } else if (palette.getLightVibrantSwatch() != null && palette.getLightVibrantSwatch().getTitleTextColor() != 0) {
            return palette.getLightVibrantSwatch().getTitleTextColor();
        } else if (palette.getLightMutedSwatch() != null && palette.getLightMutedSwatch().getTitleTextColor() != 0) {
            return palette.getLightMutedSwatch().getTitleTextColor();
        } else if (palette.getDarkVibrantSwatch() != null && palette.getDarkVibrantSwatch().getTitleTextColor() != 0) {
            return palette.getDarkVibrantSwatch().getTitleTextColor();
        } else if (palette.getDarkMutedSwatch() != null && palette.getDarkMutedSwatch().getTitleTextColor() != 0) {
            return palette.getDarkMutedSwatch().getTitleTextColor();
        } else {
            return 0;
        }
    }

    private int getBodyTextColor(Palette palette) {
        if (palette.getMutedSwatch() != null && palette.getMutedSwatch().getBodyTextColor() != 0) {
            return palette.getMutedSwatch().getBodyTextColor();
        } else if (palette.getDominantSwatch() != null && palette.getDominantSwatch().getBodyTextColor() != 0) {
            return palette.getDominantSwatch().getBodyTextColor();
        } else if (palette.getLightMutedSwatch() != null && palette.getLightMutedSwatch().getBodyTextColor() != 0) {
            return palette.getLightMutedSwatch().getBodyTextColor();
        } else if (palette.getLightVibrantSwatch() != null && palette.getLightVibrantSwatch().getBodyTextColor() != 0) {
            return palette.getLightVibrantSwatch().getBodyTextColor();
        } else if (palette.getDarkMutedSwatch() != null && palette.getDarkMutedSwatch().getBodyTextColor() != 0) {
            return palette.getDarkMutedSwatch().getBodyTextColor();
        } else if (palette.getDarkVibrantSwatch() != null && palette.getDarkVibrantSwatch().getBodyTextColor() != 0) {
            return palette.getDarkVibrantSwatch().getBodyTextColor();
        } else {
            return 0;
        }
    }
}
