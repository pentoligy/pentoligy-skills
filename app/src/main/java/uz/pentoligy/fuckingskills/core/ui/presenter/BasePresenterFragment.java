package uz.pentoligy.fuckingskills.core.ui.presenter;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import uz.pentoligy.fuckingskills.core.ui.base.BaseFragment;


/**
 * Created by pentoligy on 4/22/17.
 */

public abstract class BasePresenterFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
