package uz.pentoligy.fuckingskills.core.glide.palette;

/**
 * Created by pentoligy on 4/24/17.
 */

public interface GlidePaletteAsyncListener {
    void onGenerated(int titleTextColor, int bodyTextColor);
}