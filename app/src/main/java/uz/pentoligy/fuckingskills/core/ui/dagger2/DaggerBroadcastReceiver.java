package uz.pentoligy.fuckingskills.core.ui.dagger2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.dagger2.component.BroadcastReceiverComponent;
import uz.pentoligy.fuckingskills.dagger2.module.BroadcastReceiverModule;

/**
 * Created by pentoligy on 4/23/17.
 */

public abstract class DaggerBroadcastReceiver extends BroadcastReceiver {
    private BroadcastReceiverComponent broadcastReceiverComponent;

    @Override
    public void onReceive(Context context, Intent intent) {
        broadcastReceiverComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusBroadcastReceiverComponent(new BroadcastReceiverModule(this));

        inject(broadcastReceiverComponent);
    }

    public abstract void inject(BroadcastReceiverComponent broadcastReceiverComponent);
}
