package uz.pentoligy.fuckingskills.core.ui.dagger2;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.dagger2.component.ViewHolderComponent;
import uz.pentoligy.fuckingskills.dagger2.module.ViewHolderModule;

/**
 * Created by pentoligy on 4/26/17.
 */

public abstract class DaggerViewHolder extends RecyclerView.ViewHolder {
    private ViewHolderComponent viewHolderComponent;

    public DaggerViewHolder(View itemView) {
        super(itemView);

        viewHolderComponent = SkillsApplication.getInstance()
                .getApplicationComponent().plusViewHolderComponent(new ViewHolderModule(this));

        inject(viewHolderComponent);
    }

    protected abstract void inject(ViewHolderComponent viewHolderComponent);

}
