package uz.pentoligy.fuckingskills.core.architect;

/**
 * Created by pentoligy on 4/22/17.
 */

public interface BaseView<M> {
    void onOpenedFirst();

    void onLoading();

    void onSuccess(M items);

    void onError(String throwableMessage);

}