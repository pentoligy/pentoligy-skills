package uz.pentoligy.fuckingskills.core.ui.dagger2;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.dagger2.component.FragmentComponent;
import uz.pentoligy.fuckingskills.dagger2.module.FragmentModule;

/**
 * Created by pentoligy on 4/23/17.
 */

public abstract class DaggerFragment extends Fragment {
    private FragmentComponent fragmentComponent;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        fragmentComponent = SkillsApplication.getInstance()
                .getApplicationComponent().plusFragmentComponent(new FragmentModule(this));

        inject(fragmentComponent);
    }

    @Override
    public void onDestroy() {
        fragmentComponent = null;
        super.onDestroy();
    }

    public abstract void inject(FragmentComponent fragmentComponent);

}
