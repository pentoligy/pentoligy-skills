package uz.pentoligy.fuckingskills.core.animation;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

/**
 * Created by pentoligy on 4/25/17.
 */

public class ViewAnimation {
    private final  static int VIEW_ANIMATOR_DEFAULT_DURATION = 500;

    public static void fadeIn(boolean fromGone, View...view) {
        Animation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(VIEW_ANIMATOR_DEFAULT_DURATION);

        for (View currentView : view) {
            if (fromGone ? currentView.getVisibility() ==  View.GONE :
                    currentView.getVisibility() ==  View.INVISIBLE) {
                currentView.setAnimation(animation);
                currentView.setVisibility(View.VISIBLE);
            }
        }
    }

    public static void fadeOut(boolean toGone, View...view) {
        Animation animation = new AlphaAnimation(1.0f, 0.0f);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(VIEW_ANIMATOR_DEFAULT_DURATION);

        for (View currentView : view) {
            if (currentView.getVisibility() == View.VISIBLE) {
                currentView.setAnimation(animation);
                currentView.setVisibility(toGone ? View.GONE : View.INVISIBLE);
            }
        }
    }

    public static void fromColorToColor(int colorFrom, int colorTo, final View...view) {
        ValueAnimator valueAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        valueAnimator.setDuration(VIEW_ANIMATOR_DEFAULT_DURATION);
        valueAnimator.addUpdateListener(animator -> {

            for (View currentView : view) {
                if (currentView instanceof AppCompatImageView) {
                    ((AppCompatImageView) currentView).setColorFilter((int) animator.getAnimatedValue(), PorterDuff.Mode.SRC_ATOP);
                } else if (currentView instanceof AppCompatButton){
                    ((AppCompatButton) currentView).setTextColor((int) animator.getAnimatedValue());
                }
            }

        });

        valueAnimator.start();
    }

    public static void fromColorToColorRepeated(int colorFrom, int colorTo, AppCompatImageView imageView) {
        imageView.clearAnimation();

        ValueAnimator valueAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        valueAnimator.setDuration(VIEW_ANIMATOR_DEFAULT_DURATION * 10);
        valueAnimator.setRepeatCount(Animation.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.REVERSE);

        valueAnimator.addUpdateListener(animation ->
                imageView.setColorFilter((int) valueAnimator.getAnimatedValue(), PorterDuff.Mode.SRC_ATOP));

        valueAnimator.start();
    }

}
