package uz.pentoligy.fuckingskills.core.architect;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by pentoligy on 4/24/17.
 */

public abstract class BasePresenter<M, V extends BaseView<M>> {
    private M model;
    private V view;

    public void bindView(V view) {
        this.view = view;
    }

    public void unbindView() {
        this.view = null;
    }

    public V getView() {
        return view;
    }

    protected void setModel(M model) {
        this.model = model;
        if (setupDone()) {
            updateView();
        }
    }

    protected abstract void updateView();

    protected boolean isBinded() {
        return view != null;
    }

    public M getModel() {
        return model;
    }

    private boolean setupDone() {
        return view != null && model != null;
    }
}
