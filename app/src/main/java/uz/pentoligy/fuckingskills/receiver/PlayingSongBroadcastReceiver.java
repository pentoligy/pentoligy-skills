package uz.pentoligy.fuckingskills.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.NotificationCompat;

import java.nio.ByteBuffer;
import java.util.Arrays;

import javax.inject.Inject;

import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.core.ui.dagger2.DaggerBroadcastReceiver;
import uz.pentoligy.fuckingskills.dagger2.component.BroadcastReceiverComponent;
import uz.pentoligy.fuckingskills.ui.activity.PopupActivity;

/**
 * Created by pentoligy on 4/22/17.
 */

public class PlayingSongBroadcastReceiver extends DaggerBroadcastReceiver implements Runnable {
    private final static long DELAY_BEFORE_GETTING_MUSIC_ACTIVE_STATUS = 500;

    @Inject NotificationManager notificationManager;
    @Inject AudioManager audioManager;
    @Inject Context context;

    private Handler handler;
    private Intent intent;

    private static byte[] songInfoByteArray;

    @Override
    public void inject(BroadcastReceiverComponent broadcastReceiverComponent) {
        broadcastReceiverComponent.inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
        this.intent = intent;

        handler = new Handler();

        String receivedSongInfo = getReceivedSongInfo(intent);

        if (songInfoByteArray != null) {
            if (!Arrays.equals(songInfoByteArray, receivedSongInfo.getBytes())) {
                notificationManager.cancel(ByteBuffer.wrap(songInfoByteArray).getInt());
                songInfoByteArray = receivedSongInfo.getBytes();
                handler.postDelayed(this, DELAY_BEFORE_GETTING_MUSIC_ACTIVE_STATUS);
            }
        } else {
            songInfoByteArray = receivedSongInfo.getBytes();
            handler.postDelayed(this, DELAY_BEFORE_GETTING_MUSIC_ACTIVE_STATUS);
        }
    }

    @Override
    public void run() {
        if (audioManager.isMusicActive()) {

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context);
            Intent intent = new Intent(context, PopupActivity.class);

            intent.putExtra(PopupActivity.NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY,
                    new String[]{this.intent.getStringExtra("artist"), this.intent.getStringExtra("album"),
                            this.intent.getStringExtra("track")});

            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            notificationBuilder.setContentIntent(pendingIntent);
            notificationBuilder.setOnlyAlertOnce(true);
            notificationBuilder.setAutoCancel(true);
            notificationBuilder.setOngoing(false);
            notificationBuilder.setSmallIcon(R.drawable.ic_logo_notification);
            notificationBuilder.setDefaults(Notification.DEFAULT_ALL);
            notificationBuilder.setColor(ContextCompat.getColor(context, R.color.colorPrimary));
            notificationBuilder.setContentTitle(context.getString(R.string.notification_find_out));
            notificationBuilder.setContentText(this.intent.getStringExtra("track") +
                    context.getString(R.string.by_holder) + this.intent.getStringExtra("artist"));

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder.setPriority(Notification.PRIORITY_MAX);
            }

            Notification notification = notificationBuilder.build();

            notificationManager.notify(ByteBuffer.wrap(songInfoByteArray).getInt(), notification);
        }

        handler.removeCallbacks(this);

    }

    private String getReceivedSongInfo(Intent intent) {
        return intent.getStringExtra("artist") + intent.getStringExtra("album") + intent.getStringExtra("track");
    }
}
