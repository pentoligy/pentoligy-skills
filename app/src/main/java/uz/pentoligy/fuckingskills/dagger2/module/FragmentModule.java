package uz.pentoligy.fuckingskills.dagger2.module;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;
import uz.pentoligy.fuckingskills.dagger2.scope.FragmentScope;

/**
 * Created by pentoligy on 4/22/17.
 */

@Module
public class FragmentModule extends UnifiedModule<Fragment> {

    public FragmentModule(@NonNull Fragment module) {
        super(module);
    }

    @FragmentScope
    @Provides
    Fragment provideFragment() {
        return getModule();
    }
}
