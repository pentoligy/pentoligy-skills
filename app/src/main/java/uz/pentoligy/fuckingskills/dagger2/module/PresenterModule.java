package uz.pentoligy.fuckingskills.dagger2.module;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.architect.presenter.GeneralPresenter;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;
import uz.pentoligy.fuckingskills.dagger2.scope.PresenterScope;

/**
 * Created by pentoligy on 4/23/17.
 */

@Module
public class PresenterModule extends UnifiedModule<GeneralPresenter> {

    public PresenterModule(@NonNull GeneralPresenter module) {
        super(module);
    }

    @PresenterScope
    @Provides
    GeneralPresenter provideBasePresenter() {
        return getModule();
    }
}
