package uz.pentoligy.fuckingskills.dagger2.module;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.model.Glide;

/**
 * Created by pentoligy on 4/23/17.
 */

@Module
public class GlideModule {

    @Singleton
    @Provides
    Glide provideGlide(@NonNull Context context) {
        return new Glide(context);
    }
}