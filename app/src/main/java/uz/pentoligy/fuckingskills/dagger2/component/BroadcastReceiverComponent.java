package uz.pentoligy.fuckingskills.dagger2.component;

import dagger.Subcomponent;
import uz.pentoligy.fuckingskills.dagger2.module.BroadcastReceiverModule;
import uz.pentoligy.fuckingskills.dagger2.scope.BroadcastReceiverScope;
import uz.pentoligy.fuckingskills.receiver.PlayingSongBroadcastReceiver;

/**
 * Created by pentoligy on 4/23/17.
 */

@BroadcastReceiverScope
@Subcomponent (modules = {BroadcastReceiverModule.class})
public interface BroadcastReceiverComponent {

    void inject(PlayingSongBroadcastReceiver playingSongBroadcastReceiver);
}
