package uz.pentoligy.fuckingskills.dagger2.model;

import android.content.Context;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;

import uz.pentoligy.fuckingskills.core.glide.getter.GlideImageGetter;
import uz.pentoligy.fuckingskills.core.glide.getter.TagHandler;
import uz.pentoligy.fuckingskills.core.glide.palette.GlidePaletteAsyncListener;
import uz.pentoligy.fuckingskills.core.glide.palette.GlidePaletteBuilder;
import uz.pentoligy.fuckingskills.core.glide.transformation.BlurTransformation;
import uz.pentoligy.fuckingskills.core.glide.transformation.ColorFilterTransformation;
import uz.pentoligy.fuckingskills.core.glide.transformation.CropCircleTransformation;


/**
 * Created by pentoligy on 4/23/17.
 */

public class Glide {
    private Context context;

    public Glide(Context context) {
        this.context = context;

    }

    public DrawableTypeRequest load(String url) {
        return com.bumptech.glide.Glide.with(context).load(url);
    }

    private BitmapPool getBitmapPool() {
        return com.bumptech.glide.Glide.get(context).getBitmapPool();
    }

    public BlurTransformation getBlurTransformation() {
        return new BlurTransformation(getBitmapPool());
    }

    public CropCircleTransformation getCropCircleTransformation() {
        return new CropCircleTransformation(getBitmapPool());
    }

    public ColorFilterTransformation getColorFilterTransformation(int color) {
        return new ColorFilterTransformation(getBitmapPool(), color);
    }

    public GlideImageGetter getImageGetter(TextView textView) {
        return new GlideImageGetter(context, textView);
    }

    public TagHandler getTagHandler() {
        return new TagHandler();
    }

    public GlidePaletteBuilder getPaletteBuilder(GlidePaletteAsyncListener glidePaletteAsyncListener, ImageView imageView) {
        return new GlidePaletteBuilder(glidePaletteAsyncListener, imageView);
    }
}
