package uz.pentoligy.fuckingskills.dagger2.component;

import dagger.Subcomponent;
import uz.pentoligy.fuckingskills.core.ui.base.BaseFragment;
import uz.pentoligy.fuckingskills.dagger2.module.FragmentModule;
import uz.pentoligy.fuckingskills.ui.fragment.MainFragment;

/**
 * Created by pentoligy on 4/22/17.
 */

@Subcomponent(modules = {FragmentModule.class})
public interface FragmentComponent {

    void inject(BaseFragment baseFragment);

    void inject(MainFragment mainFragment);
}
