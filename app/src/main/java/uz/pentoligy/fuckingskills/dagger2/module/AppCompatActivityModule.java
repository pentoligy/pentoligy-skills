package uz.pentoligy.fuckingskills.dagger2.module;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;
import uz.pentoligy.fuckingskills.dagger2.scope.AppCompatActivityScope;

/**
 * Created by pentoligy on 4/22/17.
 */

@Module
public class AppCompatActivityModule extends UnifiedModule<AppCompatActivity> {

    public AppCompatActivityModule(@NonNull AppCompatActivity module) {
        super(module);
    }


    @AppCompatActivityScope
    @Provides
    AppCompatActivity provideAppCompatActivity() {
        return getModule();
    }

    @AppCompatActivityScope
    @Provides
    Context provideContext() {
        return getModule();
    }
}
