package uz.pentoligy.fuckingskills.dagger2.component;

import dagger.Subcomponent;
import uz.pentoligy.fuckingskills.core.ui.base.BaseAppCompatActivity;
import uz.pentoligy.fuckingskills.dagger2.module.AppCompatActivityModule;
import uz.pentoligy.fuckingskills.dagger2.scope.AppCompatActivityScope;
import uz.pentoligy.fuckingskills.ui.activity.MainActivity;
import uz.pentoligy.fuckingskills.ui.activity.PopupActivity;

/**
 * Created by pentoligy on 4/22/17.
 */

@AppCompatActivityScope
@Subcomponent(modules = {AppCompatActivityModule.class})
public interface AppCompatActivityComponent {

    void inject(BaseAppCompatActivity baseActivity);

    void inject(MainActivity mainActivity);

    void inject(PopupActivity popupActivity);
}
