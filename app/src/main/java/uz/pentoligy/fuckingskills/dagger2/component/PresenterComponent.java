package uz.pentoligy.fuckingskills.dagger2.component;

import dagger.Subcomponent;
import uz.pentoligy.fuckingskills.architect.presenter.activity.PopupActivityPresenter;
import uz.pentoligy.fuckingskills.dagger2.module.PresenterModule;

/**
 * Created by pentoligy on 4/23/17.
 */

@Subcomponent (modules = {PresenterModule.class})
public interface PresenterComponent {

    void inject(PopupActivityPresenter popupActivityPresenter);

}
