package uz.pentoligy.fuckingskills.dagger2.component;

import javax.inject.Singleton;

import dagger.Component;
import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.dagger2.module.AppCompatActivityModule;
import uz.pentoligy.fuckingskills.dagger2.module.ApplicationModule;
import uz.pentoligy.fuckingskills.dagger2.module.PresenterModule;
import uz.pentoligy.fuckingskills.dagger2.module.ServiceManagersModule;
import uz.pentoligy.fuckingskills.dagger2.module.BroadcastReceiverModule;
import uz.pentoligy.fuckingskills.dagger2.module.FragmentModule;
import uz.pentoligy.fuckingskills.dagger2.module.GlideModule;
import uz.pentoligy.fuckingskills.dagger2.module.NetworkModule;
import uz.pentoligy.fuckingskills.dagger2.module.ViewHolderModule;

/**
 * Created by pentoligy on 4/22/17.
 */

@Singleton
@Component(modules = {ApplicationModule.class, ServiceManagersModule.class, GlideModule.class, NetworkModule.class})
public interface ApplicationComponent {

    void inject(SkillsApplication skillsApplication);

    AppCompatActivityComponent plusAppCompatActivityComponent(AppCompatActivityModule appCompatActivityModule);

    BroadcastReceiverComponent plusBroadcastReceiverComponent(BroadcastReceiverModule broadcastReceiverModule);

    FragmentComponent plusFragmentComponent(FragmentModule fragmentModule);

    PresenterComponent plusPresenterComponent(PresenterModule presenterModule);

    ViewHolderComponent plusViewHolderComponent(ViewHolderModule viewHolderModule);
}
