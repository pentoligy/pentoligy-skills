package uz.pentoligy.fuckingskills.dagger2.module;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;

/**
 * Created by pentoligy on 4/22/17.
 */

@Module
public class ApplicationModule extends UnifiedModule<Application> {

    public ApplicationModule(@NonNull Application module) {
        super(module);
    }

    @Singleton
    @Provides
    Application provideApplication() {
        return getModule();
    }

    @Singleton
    @Provides
    Context provideContext() {
        return getModule().getApplicationContext();
    }
}
