package uz.pentoligy.fuckingskills.dagger2.component;

import dagger.Subcomponent;
import uz.pentoligy.fuckingskills.dagger2.module.ViewHolderModule;
import uz.pentoligy.fuckingskills.dagger2.scope.ViewHolderScope;
import uz.pentoligy.fuckingskills.recyclerview.viewholder.LandingViewHolder;

/**
 * Created by pentoligy on 4/26/17.
 */

@ViewHolderScope
@Subcomponent(modules = {ViewHolderModule.class})
public interface ViewHolderComponent {

    void inject(LandingViewHolder landingViewHolder);
}
