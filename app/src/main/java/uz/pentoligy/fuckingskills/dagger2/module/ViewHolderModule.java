package uz.pentoligy.fuckingskills.dagger2.module;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;
import uz.pentoligy.fuckingskills.dagger2.scope.ViewHolderScope;

/**
 * Created by pentoligy on 4/26/17.
 */

@Module
public class ViewHolderModule extends UnifiedModule<RecyclerView.ViewHolder> {

    public ViewHolderModule(@NonNull RecyclerView.ViewHolder module) {
        super(module);
    }

    @ViewHolderScope
    @Provides
    RecyclerView.ViewHolder provideViewHolder() {
        return getModule();
    }
}
