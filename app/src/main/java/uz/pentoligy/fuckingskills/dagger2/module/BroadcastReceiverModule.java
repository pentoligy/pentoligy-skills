package uz.pentoligy.fuckingskills.dagger2.module;

import android.content.BroadcastReceiver;
import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;
import uz.pentoligy.fuckingskills.dagger2.UnifiedModule;
import uz.pentoligy.fuckingskills.dagger2.scope.BroadcastReceiverScope;

/**
 * Created by pentoligy on 4/23/17.
 */

@Module
public class BroadcastReceiverModule extends UnifiedModule<BroadcastReceiver> {

    public BroadcastReceiverModule(@NonNull BroadcastReceiver module) {
        super(module);
    }

    @BroadcastReceiverScope
    @Provides
    BroadcastReceiver provideBroadcastReceiver() {
        return getModule();
    }
}
