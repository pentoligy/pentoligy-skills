package uz.pentoligy.fuckingskills.dagger2;

import android.support.annotation.NonNull;

import dagger.Module;
import dagger.Provides;

/**
 * Created by pentoligy on 4/22/17.
 */

@Module
public abstract class UnifiedModule<T> {
    private T module;

    public UnifiedModule(@NonNull T module) {
        this.module = module;
    }

    public T getModule() {
        return module;
    }
}
