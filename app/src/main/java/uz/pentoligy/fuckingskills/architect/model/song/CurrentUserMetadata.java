
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class CurrentUserMetadata implements Parcelable {

    @SerializedName("excluded_permissions")
    private List<String> mExcludedPermissions;
    @SerializedName("iq_by_action")
    private IqByAction mIqByAction;
    @SerializedName("permissions")
    private List<String> mPermissions;

    public List<String> getExcludedPermissions() {
        return mExcludedPermissions;
    }

    public void setExcludedPermissions(List<String> excludedPermissions) {
        mExcludedPermissions = excludedPermissions;
    }

    public IqByAction getIqByAction() {
        return mIqByAction;
    }

    public void setIqByAction(IqByAction iqByAction) {
        mIqByAction = iqByAction;
    }

    public List<String> getPermissions() {
        return mPermissions;
    }

    public void setPermissions(List<String> permissions) {
        mPermissions = permissions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringList(this.mExcludedPermissions);
        dest.writeParcelable(this.mIqByAction, flags);
        dest.writeStringList(this.mPermissions);
    }

    public CurrentUserMetadata() {
    }

    protected CurrentUserMetadata(Parcel in) {
        this.mExcludedPermissions = in.createStringArrayList();
        this.mIqByAction = in.readParcelable(IqByAction.class.getClassLoader());
        this.mPermissions = in.createStringArrayList();
    }

    public static final Parcelable.Creator<CurrentUserMetadata> CREATOR = new Parcelable.Creator<CurrentUserMetadata>() {
        @Override
        public CurrentUserMetadata createFromParcel(Parcel source) {
            return new CurrentUserMetadata(source);
        }

        @Override
        public CurrentUserMetadata[] newArray(int size) {
            return new CurrentUserMetadata[size];
        }
    };
}
