
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Avatar implements Parcelable {

    @SerializedName("medium")
    private Medium mMedium;
    @SerializedName("small")
    private Small mSmall;
    @SerializedName("thumb")
    private Thumb mThumb;
    @SerializedName("tiny")
    private Tiny mTiny;

    public Medium getMedium() {
        return mMedium;
    }

    public void setMedium(Medium medium) {
        mMedium = medium;
    }

    public Small getSmall() {
        return mSmall;
    }

    public void setSmall(Small small) {
        mSmall = small;
    }

    public Thumb getThumb() {
        return mThumb;
    }

    public void setThumb(Thumb thumb) {
        mThumb = thumb;
    }

    public Tiny getTiny() {
        return mTiny;
    }

    public void setTiny(Tiny tiny) {
        mTiny = tiny;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mMedium, flags);
        dest.writeParcelable(this.mSmall, flags);
        dest.writeParcelable(this.mThumb, flags);
        dest.writeParcelable(this.mTiny, flags);
    }

    public Avatar() {
    }

    protected Avatar(Parcel in) {
        this.mMedium = in.readParcelable(Medium.class.getClassLoader());
        this.mSmall = in.readParcelable(Small.class.getClassLoader());
        this.mThumb = in.readParcelable(Thumb.class.getClassLoader());
        this.mTiny = in.readParcelable(Tiny.class.getClassLoader());
    }

    public static final Parcelable.Creator<Avatar> CREATOR = new Parcelable.Creator<Avatar>() {
        @Override
        public Avatar createFromParcel(Parcel source) {
            return new Avatar(source);
        }

        @Override
        public Avatar[] newArray(int size) {
            return new Avatar[size];
        }
    };
}
