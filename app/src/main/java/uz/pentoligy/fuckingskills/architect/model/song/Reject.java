
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import uz.pentoligy.fuckingskills.architect.model.song.Primary;

public class Reject implements Parcelable {

    @SerializedName("primary")
    private Primary mPrimary;

    public Primary getPrimary() {
        return mPrimary;
    }

    public void setPrimary(Primary primary) {
        mPrimary = primary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mPrimary, flags);
    }

    public Reject() {
    }

    protected Reject(Parcel in) {
        this.mPrimary = in.readParcelable(Primary.class.getClassLoader());
    }

    public static final Parcelable.Creator<Reject> CREATOR = new Parcelable.Creator<Reject>() {
        @Override
        public Reject createFromParcel(Parcel source) {
            return new Reject(source);
        }

        @Override
        public Reject[] newArray(int size) {
            return new Reject[size];
        }
    };
}
