
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Primary implements Parcelable {

    @SerializedName("base")
    private Long mBase;
    @SerializedName("multiplier")
    private Long mMultiplier;

    public Long getBase() {
        return mBase;
    }

    public void setBase(Long base) {
        mBase = base;
    }

    public Long getMultiplier() {
        return mMultiplier;
    }

    public void setMultiplier(Long multiplier) {
        mMultiplier = multiplier;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mBase);
        dest.writeValue(this.mMultiplier);
    }

    public Primary() {
    }

    protected Primary(Parcel in) {
        this.mBase = (Long) in.readValue(Long.class.getClassLoader());
        this.mMultiplier = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Primary> CREATOR = new Parcelable.Creator<Primary>() {
        @Override
        public Primary createFromParcel(Parcel source) {
            return new Primary(source);
        }

        @Override
        public Primary[] newArray(int size) {
            return new Primary[size];
        }
    };
}
