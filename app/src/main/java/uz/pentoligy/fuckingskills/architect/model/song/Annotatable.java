
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Annotatable implements Parcelable {

    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("client_timestamps")
    private ClientTimestamps mClientTimestamps;
    @SerializedName("context")
    private String mContext;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("link_title")
    private String mLinkTitle;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("type")
    private String mType;
    @SerializedName("url")
    private String mUrl;

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public ClientTimestamps getClientTimestamps() {
        return mClientTimestamps;
    }

    public void setClientTimestamps(ClientTimestamps clientTimestamps) {
        mClientTimestamps = clientTimestamps;
    }

    public String getContext() {
        return mContext;
    }

    public void setContext(String context) {
        mContext = context;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getLinkTitle() {
        return mLinkTitle;
    }

    public void setLinkTitle(String linkTitle) {
        mLinkTitle = linkTitle;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mApiPath);
        dest.writeParcelable(this.mClientTimestamps, flags);
        dest.writeString(this.mContext);
        dest.writeValue(this.mId);
        dest.writeString(this.mImageUrl);
        dest.writeString(this.mLinkTitle);
        dest.writeString(this.mTitle);
        dest.writeString(this.mType);
        dest.writeString(this.mUrl);
    }

    public Annotatable() {
    }

    protected Annotatable(Parcel in) {
        this.mApiPath = in.readString();
        this.mClientTimestamps = in.readParcelable(ClientTimestamps.class.getClassLoader());
        this.mContext = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mImageUrl = in.readString();
        this.mLinkTitle = in.readString();
        this.mTitle = in.readString();
        this.mType = in.readString();
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<Annotatable> CREATOR = new Parcelable.Creator<Annotatable>() {
        @Override
        public Annotatable createFromParcel(Parcel source) {
            return new Annotatable(source);
        }

        @Override
        public Annotatable[] newArray(int size) {
            return new Annotatable[size];
        }
    };
}
