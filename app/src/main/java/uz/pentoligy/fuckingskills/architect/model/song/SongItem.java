package uz.pentoligy.fuckingskills.architect.model.song;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class SongItem implements Parcelable {

    @SerializedName("meta")
    private Meta mMeta;
    @SerializedName("response")
    private Response mResponse;

    public Meta getMeta() {
        return mMeta;
    }

    public void setMeta(Meta meta) {
        mMeta = meta;
    }

    public Response getResponse() {
        return mResponse;
    }

    public void setResponse(Response response) {
        mResponse = response;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mMeta, flags);
        dest.writeParcelable(this.mResponse, flags);
    }

    public SongItem() {
    }

    protected SongItem(Parcel in) {
        this.mMeta = in.readParcelable(Meta.class.getClassLoader());
        this.mResponse = in.readParcelable(Response.class.getClassLoader());
    }

    public static final Parcelable.Creator<SongItem> CREATOR = new Parcelable.Creator<SongItem>() {
        @Override
        public SongItem createFromParcel(Parcel source) {
            return new SongItem(source);
        }

        @Override
        public SongItem[] newArray(int size) {
            return new SongItem[size];
        }
    };
}
