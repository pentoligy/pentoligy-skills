
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class FactTrack implements Parcelable {

    @SerializedName("button_text")
    private String mButtonText;
    @SerializedName("external_url")
    private String mExternalUrl;
    @SerializedName("help_link_text")
    private String mHelpLinkText;
    @SerializedName("help_link_url")
    private String mHelpLinkUrl;
    @SerializedName("provider")
    private String mProvider;

    public String getButtonText() {
        return mButtonText;
    }

    public void setButtonText(String buttonText) {
        mButtonText = buttonText;
    }

    public String getExternalUrl() {
        return mExternalUrl;
    }

    public void setExternalUrl(String externalUrl) {
        mExternalUrl = externalUrl;
    }

    public String getHelpLinkText() {
        return mHelpLinkText;
    }

    public void setHelpLinkText(String helpLinkText) {
        mHelpLinkText = helpLinkText;
    }

    public String getHelpLinkUrl() {
        return mHelpLinkUrl;
    }

    public void setHelpLinkUrl(String helpLinkUrl) {
        mHelpLinkUrl = helpLinkUrl;
    }

    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String provider) {
        mProvider = provider;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mButtonText);
        dest.writeString(this.mExternalUrl);
        dest.writeString(this.mHelpLinkText);
        dest.writeString(this.mHelpLinkUrl);
        dest.writeString(this.mProvider);
    }

    public FactTrack() {
    }

    protected FactTrack(Parcel in) {
        this.mButtonText = in.readString();
        this.mExternalUrl = in.readString();
        this.mHelpLinkText = in.readString();
        this.mHelpLinkUrl = in.readString();
        this.mProvider = in.readString();
    }

    public static final Parcelable.Creator<FactTrack> CREATOR = new Parcelable.Creator<FactTrack>() {
        @Override
        public FactTrack createFromParcel(Parcel source) {
            return new FactTrack(source);
        }

        @Override
        public FactTrack[] newArray(int size) {
            return new FactTrack[size];
        }
    };
}
