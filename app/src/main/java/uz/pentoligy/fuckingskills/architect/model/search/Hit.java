
package uz.pentoligy.fuckingskills.architect.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Hit implements Parcelable {

    @SerializedName("index")
    private String mIndex;
    @SerializedName("result")
    private Result mResult;
    @SerializedName("type")
    private String mType;

    public String getIndex() {
        return mIndex;
    }

    public void setIndex(String index) {
        mIndex = index;
    }

    public Result getResult() {
        return mResult;
    }

    public void setResult(Result result) {
        mResult = result;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mIndex);
        dest.writeParcelable(this.mResult, flags);
        dest.writeString(this.mType);
    }

    public Hit() {
    }

    protected Hit(Parcel in) {
        this.mIndex = in.readString();
        this.mResult = in.readParcelable(Result.class.getClassLoader());
        this.mType = in.readString();
    }

    public static final Parcelable.Creator<Hit> CREATOR = new Parcelable.Creator<Hit>() {
        @Override
        public Hit createFromParcel(Parcel source) {
            return new Hit(source);
        }

        @Override
        public Hit[] newArray(int size) {
            return new Hit[size];
        }
    };
}
