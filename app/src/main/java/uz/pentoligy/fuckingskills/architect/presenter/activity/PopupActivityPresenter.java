package uz.pentoligy.fuckingskills.architect.presenter.activity;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import uz.pentoligy.fuckingskills.architect.model.search.Hit;
import uz.pentoligy.fuckingskills.architect.model.search.Response;
import uz.pentoligy.fuckingskills.architect.model.search.SearchItem;
import uz.pentoligy.fuckingskills.architect.model.song.Song;
import uz.pentoligy.fuckingskills.architect.model.song.SongItem;
import uz.pentoligy.fuckingskills.architect.presenter.GeneralPresenter;
import uz.pentoligy.fuckingskills.dagger2.component.PresenterComponent;
import uz.pentoligy.fuckingskills.rest.api.ApiService;

/**
 * Created by pentoligy on 4/24/17.
 */

public class PopupActivityPresenter extends GeneralPresenter<Song> {

    @Inject ApiService apiService;

    @Override
    protected void updateView() {

    }

    @Override
    protected void inject(PresenterComponent presenterComponent) {
        presenterComponent.inject(this);
    }

    @Override
    protected void loadItems(Object... objects) {
        String artist = (String) objects[0];
        String song = (String) objects[1];

        apiService.getSearchItem(artist + " " + song)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(SearchItem::getResponse)
                .map(Response::getHits)
                .flatMapIterable(hits -> hits)
                .takeWhile(hit -> hit.getResult().getTitle().toLowerCase().equals(song.toLowerCase()))
                .map(Hit::getResult)
                .flatMap(result -> apiService.getSongItem(result.getId())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(SongItem::getResponse)
                        .map(uz.pentoligy.fuckingskills.architect.model.song.Response::getSong)
                        .doOnNext(this::setModel)
                        .doOnError(throwable -> actionOnFailure(throwable == null ? ERROR_UNKNOWN : throwable.getMessage())))
                .subscribe(songItem -> Log.d("ON NEXT: TITLE IS", songItem.getTitle()),
                        throwable -> actionOnFailure(throwable == null ?
                                ERROR_UNKNOWN : throwable.getMessage()), () -> {
                            if (getModel() != null) {
                                actionOnSuccess(getModel());
                            } else {
                                actionOnLyricsNotFound();
                            }
                        });

    }

}
