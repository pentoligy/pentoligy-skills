package uz.pentoligy.fuckingskills.architect.presenter;

import android.os.Bundle;
import android.os.Parcelable;

import uz.pentoligy.SkillsApplication;
import uz.pentoligy.fuckingskills.architect.view.BaseActivityView;
import uz.pentoligy.fuckingskills.core.architect.BasePresenter;
import uz.pentoligy.fuckingskills.dagger2.component.PresenterComponent;
import uz.pentoligy.fuckingskills.dagger2.module.PresenterModule;

/**
 * Created by pentoligy on 4/24/17.
 */

public abstract class GeneralPresenter<M extends Parcelable> extends BasePresenter<M, BaseActivityView<M>> {
    private final static String BUNDLE_ITEM_SAVED_INSTANCE_TAG = "presenter_saved_instance_item";
    private final static String BUNDLE_STRING_SAVED_INSTANCE_TAG = "presenter_saved_instance_throwable_message";

    private final static String BUNDLE_BOOLEAN_ISOPENEDFIRST_SAVED_INSTANCE_TAG = "presenter_saved_instance_boolean_isopenedfirst";
    private final static String BUNDLE_BOOLEAN_ISLOADING_SAVED_INSTANCE_TAG = "presenter_saved_instance_boolean_isloading";
    private final static String BUNDLE_BOOLEAN_ISSUCCESSED_SAVED_INSTANCE_TAG = "presenter_saved_instance_boolean_issuccessed";
    private final static String BUNDLE_BOOLEAN_ISERROR_SAVED_INSTANCE_TAG = "presenter_saved_instance_boolean_iserror";

    public final static String ERROR_LYRICS_NOT_FOUND = "LYRICS_NOT_FOUND_ERROR";
    protected final static String ERROR_UNKNOWN = "UNKNOWN_ERROR";

    private PresenterComponent presenterComponent;

    private boolean isOpenedFirst;
    private boolean isLoading;
    private boolean isSuccessed;
    private boolean isError;
    private String throwableMessage;

    @Override
    public void bindView(BaseActivityView<M> view) {
        presenterComponent = SkillsApplication.getInstance()
                .getApplicationComponent()
                .plusPresenterComponent(new PresenterModule(this));

        inject(presenterComponent);

        super.bindView(view);

        if (isOpenedFirst && !isError) {
            view.onOpenedFirst();
        } else if (isLoading) {
            view.onLoading();
        } else if (isError) {
            view.onError(throwableMessage == null ? null : throwableMessage);
        }
    }

    @Override
    public void unbindView() {
        presenterComponent = null;
        super.unbindView();

    }

    public void getItems(Object... objects) {
        isOpenedFirst = false;
        isLoading = true;
        isSuccessed = false;
        isError = false;

        loadItems(objects);

        getView().onLoading();
    }

    protected void actionOnSuccess(M item) {
        isLoading = false;
        isSuccessed = true;
        isError = false;

        setModel(item);

        if (isBinded()) {
            getView().onSuccess(item);
            updateView();
        }
    }

    protected void actionOnFailure(String throwableMessage) {
        this.throwableMessage = throwableMessage;

        if (getModel() == null) {
            isOpenedFirst = true;
        }

        isLoading = false;
        isSuccessed = false;
        isError = true;

        if (isBinded()) {
            getView().onError(throwableMessage);
            updateView();
        }
    }

    protected void actionOnLyricsNotFound() {
        this.throwableMessage = ERROR_LYRICS_NOT_FOUND;

        isLoading = false;
        isSuccessed = false;
        isError = true;

        if (isBinded()) {
            getView().onError(throwableMessage);
            updateView();
        }
    }

    public void saveState(Bundle savedInstanceState) {
        if (getModel() != null) {
            savedInstanceState.putParcelable(BUNDLE_ITEM_SAVED_INSTANCE_TAG, getModel());
        }

        savedInstanceState.putBoolean(BUNDLE_BOOLEAN_ISOPENEDFIRST_SAVED_INSTANCE_TAG, isOpenedFirst);
        savedInstanceState.putBoolean(BUNDLE_BOOLEAN_ISLOADING_SAVED_INSTANCE_TAG, isLoading);
        savedInstanceState.putBoolean(BUNDLE_BOOLEAN_ISSUCCESSED_SAVED_INSTANCE_TAG, isSuccessed);
        savedInstanceState.putBoolean(BUNDLE_BOOLEAN_ISERROR_SAVED_INSTANCE_TAG, isError);
        savedInstanceState.putString(BUNDLE_STRING_SAVED_INSTANCE_TAG, throwableMessage);
    }

    public void restoreState(Bundle outState) {
        if (outState.getParcelable(BUNDLE_ITEM_SAVED_INSTANCE_TAG) != null) {
            setModel((outState.getParcelable(BUNDLE_ITEM_SAVED_INSTANCE_TAG)));
        }

        isOpenedFirst = outState.getBoolean(BUNDLE_BOOLEAN_ISOPENEDFIRST_SAVED_INSTANCE_TAG);
        isLoading = outState.getBoolean(BUNDLE_BOOLEAN_ISLOADING_SAVED_INSTANCE_TAG);
        isSuccessed = outState.getBoolean(BUNDLE_BOOLEAN_ISSUCCESSED_SAVED_INSTANCE_TAG);
        isError = outState.getBoolean(BUNDLE_BOOLEAN_ISERROR_SAVED_INSTANCE_TAG);
        throwableMessage = outState.getString(BUNDLE_STRING_SAVED_INSTANCE_TAG);
    }

    protected abstract void inject(PresenterComponent presenterComponent);

    protected abstract void loadItems(Object... objects);
}
