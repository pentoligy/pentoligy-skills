
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("avatar")
    private Avatar mAvatar;
    @SerializedName("current_user_metadata")
    private CurrentUserMetadata mCurrentUserMetadata;
    @SerializedName("header_image_url")
    private String mHeaderImageUrl;
    @SerializedName("human_readable_role_for_display")
    private String mHumanReadableRoleForDisplay;
    @SerializedName("id")
    private Long mId;
    @SerializedName("iq")
    private Long mIq;
    @SerializedName("login")
    private String mLogin;
    @SerializedName("name")
    private String mName;
    @SerializedName("role_for_display")
    private String mRoleForDisplay;
    @SerializedName("url")
    private String mUrl;

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public Avatar getAvatar() {
        return mAvatar;
    }

    public void setAvatar(Avatar avatar) {
        mAvatar = avatar;
    }

    public CurrentUserMetadata getCurrentUserMetadata() {
        return mCurrentUserMetadata;
    }

    public void setCurrentUserMetadata(CurrentUserMetadata currentUserMetadata) {
        mCurrentUserMetadata = currentUserMetadata;
    }

    public String getHeaderImageUrl() {
        return mHeaderImageUrl;
    }

    public void setHeaderImageUrl(String headerImageUrl) {
        mHeaderImageUrl = headerImageUrl;
    }

    public String getHumanReadableRoleForDisplay() {
        return mHumanReadableRoleForDisplay;
    }

    public void setHumanReadableRoleForDisplay(String humanReadableRoleForDisplay) {
        mHumanReadableRoleForDisplay = humanReadableRoleForDisplay;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getIq() {
        return mIq;
    }

    public void setIq(Long iq) {
        mIq = iq;
    }

    public String getLogin() {
        return mLogin;
    }

    public void setLogin(String login) {
        mLogin = login;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getRoleForDisplay() {
        return mRoleForDisplay;
    }

    public void setRoleForDisplay(String roleForDisplay) {
        mRoleForDisplay = roleForDisplay;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mApiPath);
        dest.writeParcelable(this.mAvatar, flags);
        dest.writeParcelable(this.mCurrentUserMetadata, flags);
        dest.writeString(this.mHeaderImageUrl);
        dest.writeString(this.mHumanReadableRoleForDisplay);
        dest.writeValue(this.mId);
        dest.writeValue(this.mIq);
        dest.writeString(this.mLogin);
        dest.writeString(this.mName);
        dest.writeString(this.mRoleForDisplay);
        dest.writeString(this.mUrl);
    }

    public User() {
    }

    protected User(Parcel in) {
        this.mApiPath = in.readString();
        this.mAvatar = in.readParcelable(Avatar.class.getClassLoader());
        this.mCurrentUserMetadata = in.readParcelable(CurrentUserMetadata.class.getClassLoader());
        this.mHeaderImageUrl = in.readString();
        this.mHumanReadableRoleForDisplay = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mIq = (Long) in.readValue(Long.class.getClassLoader());
        this.mLogin = in.readString();
        this.mName = in.readString();
        this.mRoleForDisplay = in.readString();
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<User> CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
