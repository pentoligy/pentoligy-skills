
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class IqByAction implements Parcelable {

    @SerializedName("accept")
    private Accept mAccept;
    @SerializedName("delete")
    private Delete mDelete;
    @SerializedName("edit_metadata")
    private EditMetadata mEditMetadata;
    @SerializedName("reject")
    private Reject mReject;

    public Accept getAccept() {
        return mAccept;
    }

    public void setAccept(Accept accept) {
        mAccept = accept;
    }

    public Delete getDelete() {
        return mDelete;
    }

    public void setDelete(Delete delete) {
        mDelete = delete;
    }

    public EditMetadata getEditMetadata() {
        return mEditMetadata;
    }

    public void setEditMetadata(EditMetadata editMetadata) {
        mEditMetadata = editMetadata;
    }

    public Reject getReject() {
        return mReject;
    }

    public void setReject(Reject reject) {
        mReject = reject;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mAccept, flags);
        dest.writeParcelable(this.mDelete, flags);
        dest.writeParcelable(this.mEditMetadata, flags);
        dest.writeParcelable(this.mReject, flags);
    }

    public IqByAction() {
    }

    protected IqByAction(Parcel in) {
        this.mAccept = in.readParcelable(Accept.class.getClassLoader());
        this.mDelete = in.readParcelable(Delete.class.getClassLoader());
        this.mEditMetadata = in.readParcelable(EditMetadata.class.getClassLoader());
        this.mReject = in.readParcelable(Reject.class.getClassLoader());
    }

    public static final Parcelable.Creator<IqByAction> CREATOR = new Parcelable.Creator<IqByAction>() {
        @Override
        public IqByAction createFromParcel(Parcel source) {
            return new IqByAction(source);
        }

        @Override
        public IqByAction[] newArray(int size) {
            return new IqByAction[size];
        }
    };
}
