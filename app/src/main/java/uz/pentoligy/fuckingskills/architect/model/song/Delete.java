
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Delete implements Parcelable {

    @SerializedName("primary")
    private Primary mPrimary;

    public Primary getPrimary() {
        return mPrimary;
    }

    public void setPrimary(Primary primary) {
        mPrimary = primary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mPrimary, flags);
    }

    public Delete() {
    }

    protected Delete(Parcel in) {
        this.mPrimary = in.readParcelable(Primary.class.getClassLoader());
    }

    public static final Parcelable.Creator<Delete> CREATOR = new Parcelable.Creator<Delete>() {
        @Override
        public Delete createFromParcel(Parcel source) {
            return new Delete(source);
        }

        @Override
        public Delete[] newArray(int size) {
            return new Delete[size];
        }
    };
}
