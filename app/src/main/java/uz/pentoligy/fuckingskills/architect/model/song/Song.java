package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Song implements Parcelable {

    @SerializedName("album")
    private Album mAlbum;
    @SerializedName("annotation_count")
    private Long mAnnotationCount;
    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("current_user_metadata")
    private CurrentUserMetadata mCurrentUserMetadata;
    @SerializedName("description")
    private Description mDescription;
    @SerializedName("description_annotation")
    private DescriptionAnnotation mDescriptionAnnotation;
    @SerializedName("embed_content")
    private String mEmbedContent;
    @SerializedName("fact_track")
    private FactTrack mFactTrack;
    @SerializedName("featured_artists")
    private List<FeaturedArtist> mFeaturedArtists;
    @SerializedName("full_title")
    private String mFullTitle;
    @SerializedName("header_image_thumbnail_url")
    private String mHeaderImageThumbnailUrl;
    @SerializedName("header_image_url")
    private String mHeaderImageUrl;
    @SerializedName("id")
    private Long mId;
    @SerializedName("lyrics_owner_id")
    private Long mLyricsOwnerId;
    @SerializedName("media")
    private List<Medium> mMedia;
    @SerializedName("path")
    private String mPath;
    @SerializedName("primary_artist")
    private PrimaryArtist mPrimaryArtist;
    @SerializedName("producer_artists")
    private List<ProducerArtist> mProducerArtists;
    @SerializedName("pyongs_count")
    private Long mPyongsCount;
    @SerializedName("recording_location")
    private String mRecordingLocation;
    @SerializedName("release_date")
    private String mReleaseDate;
    @SerializedName("song_art_image_thumbnail_url")
    private String mSongArtImageThumbnailUrl;
    @SerializedName("song_art_image_url")
    private String mSongArtImageUrl;
    @SerializedName("song_relationships")
    private List<SongRelationship> mSongRelationships;
    @SerializedName("stats")
    private Stats mStats;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("verified_annotations_by")
    private List<VerifiedAnnotationsBy> mVerifiedAnnotationsBy;
    @SerializedName("verified_contributors")
    private List<VerifiedContributor> mVerifiedContributors;
    @SerializedName("writer_artists")
    private List<WriterArtist> mWriterArtists;

    public Album getAlbum() {
        return mAlbum;
    }

    public void setAlbum(Album album) {
        mAlbum = album;
    }

    public Long getAnnotationCount() {
        return mAnnotationCount;
    }

    public void setAnnotationCount(Long annotationCount) {
        mAnnotationCount = annotationCount;
    }

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public CurrentUserMetadata getCurrentUserMetadata() {
        return mCurrentUserMetadata;
    }

    public void setCurrentUserMetadata(CurrentUserMetadata currentUserMetadata) {
        mCurrentUserMetadata = currentUserMetadata;
    }

    public Description getDescription() {
        return mDescription;
    }

    public void setDescription(Description description) {
        mDescription = description;
    }

    public DescriptionAnnotation getDescriptionAnnotation() {
        return mDescriptionAnnotation;
    }

    public void setDescriptionAnnotation(DescriptionAnnotation descriptionAnnotation) {
        mDescriptionAnnotation = descriptionAnnotation;
    }

    public String getEmbedContent() {
        return mEmbedContent;
    }

    public void setEmbedContent(String embedContent) {
        mEmbedContent = embedContent;
    }

    public FactTrack getFactTrack() {
        return mFactTrack;
    }

    public void setFactTrack(FactTrack factTrack) {
        mFactTrack = factTrack;
    }

    public String getFullTitle() {
        return mFullTitle;
    }

    public void setFullTitle(String fullTitle) {
        mFullTitle = fullTitle;
    }

    public String getHeaderImageThumbnailUrl() {
        return mHeaderImageThumbnailUrl;
    }

    public void setHeaderImageThumbnailUrl(String headerImageThumbnailUrl) {
        mHeaderImageThumbnailUrl = headerImageThumbnailUrl;
    }

    public String getHeaderImageUrl() {
        return mHeaderImageUrl;
    }

    public void setHeaderImageUrl(String headerImageUrl) {
        mHeaderImageUrl = headerImageUrl;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getLyricsOwnerId() {
        return mLyricsOwnerId;
    }

    public void setLyricsOwnerId(Long lyricsOwnerId) {
        mLyricsOwnerId = lyricsOwnerId;
    }

    public List<Medium> getMedia() {
        return mMedia;
    }

    public void setMedia(List<Medium> media) {
        mMedia = media;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public PrimaryArtist getPrimaryArtist() {
        return mPrimaryArtist;
    }

    public void setPrimaryArtist(PrimaryArtist primaryArtist) {
        mPrimaryArtist = primaryArtist;
    }

    public List<ProducerArtist> getProducerArtists() {
        return mProducerArtists;
    }

    public void setProducerArtists(List<ProducerArtist> producerArtists) {
        mProducerArtists = producerArtists;
    }

    public Long getPyongsCount() {
        return mPyongsCount;
    }

    public void setPyongsCount(Long pyongsCount) {
        mPyongsCount = pyongsCount;
    }

    public String getRecordingLocation() {
        return mRecordingLocation;
    }

    public void setRecordingLocation(String recordingLocation) {
        mRecordingLocation = recordingLocation;
    }

    public String getReleaseDate() {
        return mReleaseDate;
    }

    public String getReleaseDateInWords() {
        try {
            SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
            Date date = inputDateFormat.parse(mReleaseDate);
            SimpleDateFormat outputDateFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.US);
            return outputDateFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void setReleaseDate(String releaseDate) {
        mReleaseDate = releaseDate;
    }

    public String getSongArtImageThumbnailUrl() {
        return mSongArtImageThumbnailUrl;
    }

    public void setSongArtImageThumbnailUrl(String songArtImageThumbnailUrl) {
        mSongArtImageThumbnailUrl = songArtImageThumbnailUrl;
    }

    public String getSongArtImageUrl() {
        return mSongArtImageUrl;
    }

    public void setSongArtImageUrl(String songArtImageUrl) {
        mSongArtImageUrl = songArtImageUrl;
    }

    public List<SongRelationship> getSongRelationships() {
        return mSongRelationships;
    }

    public void setSongRelationships(List<SongRelationship> songRelationships) {
        mSongRelationships = songRelationships;
    }

    public Stats getStats() {
        return mStats;
    }

    public void setStats(Stats stats) {
        mStats = stats;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public List<VerifiedAnnotationsBy> getVerifiedAnnotationsBy() {
        return mVerifiedAnnotationsBy;
    }

    public void setVerifiedAnnotationsBy(List<VerifiedAnnotationsBy> verifiedAnnotationsBy) {
        mVerifiedAnnotationsBy = verifiedAnnotationsBy;
    }

    public List<VerifiedContributor> getVerifiedContributors() {
        return mVerifiedContributors;
    }

    public void setVerifiedContributors(List<VerifiedContributor> verifiedContributors) {
        mVerifiedContributors = verifiedContributors;
    }

    private List<WriterArtist> getWriterArtists() {
        return mWriterArtists;
    }

    public void setWriterArtists(List<WriterArtist> writerArtists) {
        mWriterArtists = writerArtists;
    }

    public String getWriters() {
        String writers = "";
        List<WriterArtist> writerArtists = mWriterArtists;

        for (int i = 0; i < writerArtists.size(); i++) {
            writers = writers + writerArtists.get(i).toString() + " ";
        }

        return writers;
    }

    public String getSongArtUrl() {
        return !mSongArtImageThumbnailUrl.contains("default_cover_image") ?
                mSongArtImageThumbnailUrl : mAlbum.getCoverArtUrl();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mAlbum, flags);
        dest.writeValue(this.mAnnotationCount);
        dest.writeString(this.mApiPath);
        dest.writeParcelable(this.mCurrentUserMetadata, flags);
        dest.writeParcelable(this.mDescription, flags);
        dest.writeParcelable(this.mDescriptionAnnotation, flags);
        dest.writeString(this.mEmbedContent);
        dest.writeParcelable(this.mFactTrack, flags);
        dest.writeList(this.mFeaturedArtists);
        dest.writeString(this.mFullTitle);
        dest.writeString(this.mHeaderImageThumbnailUrl);
        dest.writeString(this.mHeaderImageUrl);
        dest.writeValue(this.mId);
        dest.writeValue(this.mLyricsOwnerId);
        dest.writeTypedList(this.mMedia);
        dest.writeString(this.mPath);
        dest.writeParcelable(this.mPrimaryArtist, flags);
        dest.writeTypedList(this.mProducerArtists);
        dest.writeValue(this.mPyongsCount);
        dest.writeString(this.mRecordingLocation);
        dest.writeString(this.mReleaseDate);
        dest.writeString(this.mSongArtImageThumbnailUrl);
        dest.writeString(this.mSongArtImageUrl);
        dest.writeList(this.mSongRelationships);
        dest.writeParcelable(this.mStats, flags);
        dest.writeString(this.mTitle);
        dest.writeString(this.mUrl);
        dest.writeList(this.mVerifiedAnnotationsBy);
        dest.writeList(this.mVerifiedContributors);
        dest.writeList(this.mWriterArtists);
    }

    public Song() {
    }

    protected Song(Parcel in) {
        this.mAlbum = in.readParcelable(Album.class.getClassLoader());
        this.mAnnotationCount = (Long) in.readValue(Long.class.getClassLoader());
        this.mApiPath = in.readString();
        this.mCurrentUserMetadata = in.readParcelable(CurrentUserMetadata.class.getClassLoader());
        this.mDescription = in.readParcelable(Description.class.getClassLoader());
        this.mDescriptionAnnotation = in.readParcelable(DescriptionAnnotation.class.getClassLoader());
        this.mEmbedContent = in.readString();
        this.mFactTrack = in.readParcelable(FactTrack.class.getClassLoader());
        this.mFeaturedArtists = new ArrayList<FeaturedArtist>();
        in.readList(this.mFeaturedArtists, FeaturedArtist.class.getClassLoader());
        this.mFullTitle = in.readString();
        this.mHeaderImageThumbnailUrl = in.readString();
        this.mHeaderImageUrl = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mLyricsOwnerId = (Long) in.readValue(Long.class.getClassLoader());
        this.mMedia = in.createTypedArrayList(Medium.CREATOR);
        this.mPath = in.readString();
        this.mPrimaryArtist = in.readParcelable(PrimaryArtist.class.getClassLoader());
        this.mProducerArtists = in.createTypedArrayList(ProducerArtist.CREATOR);
        this.mPyongsCount = (Long) in.readValue(Long.class.getClassLoader());
        this.mRecordingLocation = in.readString();
        this.mReleaseDate = in.readString();
        this.mSongArtImageThumbnailUrl = in.readString();
        this.mSongArtImageUrl = in.readString();
        this.mSongRelationships = new ArrayList<SongRelationship>();
        in.readList(this.mSongRelationships, SongRelationship.class.getClassLoader());
        this.mStats = in.readParcelable(Stats.class.getClassLoader());
        this.mTitle = in.readString();
        this.mUrl = in.readString();
        this.mVerifiedAnnotationsBy = new ArrayList<VerifiedAnnotationsBy>();
        in.readList(this.mVerifiedAnnotationsBy, VerifiedAnnotationsBy.class.getClassLoader());
        this.mVerifiedContributors = new ArrayList<VerifiedContributor>();
        in.readList(this.mVerifiedContributors, VerifiedContributor.class.getClassLoader());
        this.mWriterArtists = new ArrayList<WriterArtist>();
        in.readList(this.mWriterArtists, WriterArtist.class.getClassLoader());
    }

    public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
        @Override
        public Song createFromParcel(Parcel source) {
            return new Song(source);
        }

        @Override
        public Song[] newArray(int size) {
            return new Song[size];
        }
    };
}
