
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Author implements Parcelable {

    @SerializedName("attribution")
    private Double mAttribution;
    @SerializedName("user")
    private User mUser;

    public Double getAttribution() {
        return mAttribution;
    }

    public void setAttribution(Double attribution) {
        mAttribution = attribution;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mAttribution);
        dest.writeParcelable(this.mUser, flags);
    }

    public Author() {
    }

    protected Author(Parcel in) {
        this.mAttribution = (Double) in.readValue(Double.class.getClassLoader());
        this.mUser = in.readParcelable(User.class.getClassLoader());
    }

    public static final Parcelable.Creator<Author> CREATOR = new Parcelable.Creator<Author>() {
        @Override
        public Author createFromParcel(Parcel source) {
            return new Author(source);
        }

        @Override
        public Author[] newArray(int size) {
            return new Author[size];
        }
    };
}
