
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Stats implements Parcelable {

    @SerializedName("accepted_annotations")
    private Long mAcceptedAnnotations;
    @SerializedName("contributors")
    private Long mContributors;
    @SerializedName("iq_earners")
    private Long mIqEarners;
    @SerializedName("pageviews")
    private Long mPageviews;
    @SerializedName("transcribers")
    private Long mTranscribers;
    @SerializedName("unreviewed_annotations")
    private Long mUnreviewedAnnotations;
    @SerializedName("verified_annotations")
    private Long mVerifiedAnnotations;

    public Long getAcceptedAnnotations() {
        return mAcceptedAnnotations;
    }

    public void setAcceptedAnnotations(Long acceptedAnnotations) {
        mAcceptedAnnotations = acceptedAnnotations;
    }

    public Long getContributors() {
        return mContributors;
    }

    public void setContributors(Long contributors) {
        mContributors = contributors;
    }

    public Long getIqEarners() {
        return mIqEarners;
    }

    public void setIqEarners(Long iqEarners) {
        mIqEarners = iqEarners;
    }

    public Long getPageviews() {
        return mPageviews;
    }

    public void setPageviews(Long pageviews) {
        mPageviews = pageviews;
    }

    public Long getTranscribers() {
        return mTranscribers;
    }

    public void setTranscribers(Long transcribers) {
        mTranscribers = transcribers;
    }

    public Long getUnreviewedAnnotations() {
        return mUnreviewedAnnotations;
    }

    public void setUnreviewedAnnotations(Long unreviewedAnnotations) {
        mUnreviewedAnnotations = unreviewedAnnotations;
    }

    public Long getVerifiedAnnotations() {
        return mVerifiedAnnotations;
    }

    public void setVerifiedAnnotations(Long verifiedAnnotations) {
        mVerifiedAnnotations = verifiedAnnotations;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mAcceptedAnnotations);
        dest.writeValue(this.mContributors);
        dest.writeValue(this.mIqEarners);
        dest.writeValue(this.mPageviews);
        dest.writeValue(this.mTranscribers);
        dest.writeValue(this.mUnreviewedAnnotations);
        dest.writeValue(this.mVerifiedAnnotations);
    }

    public Stats() {
    }

    protected Stats(Parcel in) {
        this.mAcceptedAnnotations = (Long) in.readValue(Long.class.getClassLoader());
        this.mContributors = (Long) in.readValue(Long.class.getClassLoader());
        this.mIqEarners = (Long) in.readValue(Long.class.getClassLoader());
        this.mPageviews = (Long) in.readValue(Long.class.getClassLoader());
        this.mTranscribers = (Long) in.readValue(Long.class.getClassLoader());
        this.mUnreviewedAnnotations = (Long) in.readValue(Long.class.getClassLoader());
        this.mVerifiedAnnotations = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {
        @Override
        public Stats createFromParcel(Parcel source) {
            return new Stats(source);
        }

        @Override
        public Stats[] newArray(int size) {
            return new Stats[size];
        }
    };
}
