
package uz.pentoligy.fuckingskills.architect.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Result implements Parcelable {

    @SerializedName("annotation_count")
    private Long mAnnotationCount;
    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("full_title")
    private String mFullTitle;
    @SerializedName("header_image_thumbnail_url")
    private String mHeaderImageThumbnailUrl;
    @SerializedName("header_image_url")
    private String mHeaderImageUrl;
    @SerializedName("id")
    private Long mId;
    @SerializedName("lyrics_owner_id")
    private Long mLyricsOwnerId;
    @SerializedName("path")
    private String mPath;
    @SerializedName("primary_artist")
    private PrimaryArtist mPrimaryArtist;
    @SerializedName("pyongs_count")
    private Long mPyongsCount;
    @SerializedName("song_art_image_thumbnail_url")
    private String mSongArtImageThumbnailUrl;
    @SerializedName("stats")
    private Stats mStats;
    @SerializedName("title")
    private String mTitle;
    @SerializedName("url")
    private String mUrl;

    public Long getAnnotationCount() {
        return mAnnotationCount;
    }

    public void setAnnotationCount(Long annotationCount) {
        mAnnotationCount = annotationCount;
    }

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public String getFullTitle() {
        return mFullTitle;
    }

    public void setFullTitle(String fullTitle) {
        mFullTitle = fullTitle;
    }

    public String getHeaderImageThumbnailUrl() {
        return mHeaderImageThumbnailUrl;
    }

    public void setHeaderImageThumbnailUrl(String headerImageThumbnailUrl) {
        mHeaderImageThumbnailUrl = headerImageThumbnailUrl;
    }

    public String getHeaderImageUrl() {
        return mHeaderImageUrl;
    }

    public void setHeaderImageUrl(String headerImageUrl) {
        mHeaderImageUrl = headerImageUrl;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public Long getLyricsOwnerId() {
        return mLyricsOwnerId;
    }

    public void setLyricsOwnerId(Long lyricsOwnerId) {
        mLyricsOwnerId = lyricsOwnerId;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public PrimaryArtist getPrimaryArtist() {
        return mPrimaryArtist;
    }

    public void setPrimaryArtist(PrimaryArtist primaryArtist) {
        mPrimaryArtist = primaryArtist;
    }

    public Long getPyongsCount() {
        return mPyongsCount;
    }

    public void setPyongsCount(Long pyongsCount) {
        mPyongsCount = pyongsCount;
    }

    public String getSongArtImageThumbnailUrl() {
        return mSongArtImageThumbnailUrl;
    }

    public void setSongArtImageThumbnailUrl(String songArtImageThumbnailUrl) {
        mSongArtImageThumbnailUrl = songArtImageThumbnailUrl;
    }

    public Stats getStats() {
        return mStats;
    }

    public void setStats(Stats stats) {
        mStats = stats;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mAnnotationCount);
        dest.writeString(this.mApiPath);
        dest.writeString(this.mFullTitle);
        dest.writeString(this.mHeaderImageThumbnailUrl);
        dest.writeString(this.mHeaderImageUrl);
        dest.writeValue(this.mId);
        dest.writeValue(this.mLyricsOwnerId);
        dest.writeString(this.mPath);
        dest.writeParcelable(this.mPrimaryArtist, flags);
        dest.writeValue(this.mPyongsCount);
        dest.writeString(this.mSongArtImageThumbnailUrl);
        dest.writeParcelable(this.mStats, flags);
        dest.writeString(this.mTitle);
        dest.writeString(this.mUrl);
    }

    public Result() {
    }

    protected Result(Parcel in) {
        this.mAnnotationCount = (Long) in.readValue(Long.class.getClassLoader());
        this.mApiPath = in.readString();
        this.mFullTitle = in.readString();
        this.mHeaderImageThumbnailUrl = in.readString();
        this.mHeaderImageUrl = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mLyricsOwnerId = (Long) in.readValue(Long.class.getClassLoader());
        this.mPath = in.readString();
        this.mPrimaryArtist = in.readParcelable(PrimaryArtist.class.getClassLoader());
        this.mPyongsCount = (Long) in.readValue(Long.class.getClassLoader());
        this.mSongArtImageThumbnailUrl = in.readString();
        this.mStats = in.readParcelable(Stats.class.getClassLoader());
        this.mTitle = in.readString();
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<Result> CREATOR = new Parcelable.Creator<Result>() {
        @Override
        public Result createFromParcel(Parcel source) {
            return new Result(source);
        }

        @Override
        public Result[] newArray(int size) {
            return new Result[size];
        }
    };
}
