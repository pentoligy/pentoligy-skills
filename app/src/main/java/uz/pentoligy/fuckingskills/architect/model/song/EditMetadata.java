
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class EditMetadata implements Parcelable {

    @SerializedName("primary")
    private Primary mPrimary;

    public Primary getPrimary() {
        return mPrimary;
    }

    public void setPrimary(Primary primary) {
        mPrimary = primary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mPrimary, flags);
    }

    public EditMetadata() {
    }

    protected EditMetadata(Parcel in) {
        this.mPrimary = in.readParcelable(Primary.class.getClassLoader());
    }

    public static final Parcelable.Creator<EditMetadata> CREATOR = new Parcelable.Creator<EditMetadata>() {
        @Override
        public EditMetadata createFromParcel(Parcel source) {
            return new EditMetadata(source);
        }

        @Override
        public EditMetadata[] newArray(int size) {
            return new EditMetadata[size];
        }
    };
}
