
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Medium implements Parcelable {

    @SerializedName("bounding_box")
    private BoundingBox mBoundingBox;
    @SerializedName("native_uri")
    private String mNativeUri;
    @SerializedName("provider")
    private String mProvider;
    @SerializedName("provider_id")
    private String mProviderId;
    @SerializedName("start")
    private Long mStart;
    @SerializedName("type")
    private String mType;
    @SerializedName("url")
    private String mUrl;

    public BoundingBox getBoundingBox() {
        return mBoundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        mBoundingBox = boundingBox;
    }

    public String getNativeUri() {
        return mNativeUri;
    }

    public void setNativeUri(String nativeUri) {
        mNativeUri = nativeUri;
    }

    public String getProvider() {
        return mProvider;
    }

    public void setProvider(String provider) {
        mProvider = provider;
    }

    public String getProviderId() {
        return mProviderId;
    }

    public void setProviderId(String providerId) {
        mProviderId = providerId;
    }

    public Long getStart() {
        return mStart;
    }

    public void setStart(Long start) {
        mStart = start;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mBoundingBox, flags);
        dest.writeString(this.mNativeUri);
        dest.writeString(this.mProvider);
        dest.writeString(this.mProviderId);
        dest.writeValue(this.mStart);
        dest.writeString(this.mType);
        dest.writeString(this.mUrl);
    }

    public Medium() {
    }

    protected Medium(Parcel in) {
        this.mBoundingBox = in.readParcelable(BoundingBox.class.getClassLoader());
        this.mNativeUri = in.readString();
        this.mProvider = in.readString();
        this.mProviderId = in.readString();
        this.mStart = (Long) in.readValue(Long.class.getClassLoader());
        this.mType = in.readString();
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<Medium> CREATOR = new Parcelable.Creator<Medium>() {
        @Override
        public Medium createFromParcel(Parcel source) {
            return new Medium(source);
        }

        @Override
        public Medium[] newArray(int size) {
            return new Medium[size];
        }
    };
}
