
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Tiny implements Parcelable {

    @SerializedName("bounding_box")
    private BoundingBox mBoundingBox;
    @SerializedName("url")
    private String mUrl;

    public BoundingBox getBoundingBox() {
        return mBoundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        mBoundingBox = boundingBox;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mBoundingBox, flags);
        dest.writeString(this.mUrl);
    }

    public Tiny() {
    }

    protected Tiny(Parcel in) {
        this.mBoundingBox = in.readParcelable(BoundingBox.class.getClassLoader());
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<Tiny> CREATOR = new Parcelable.Creator<Tiny>() {
        @Override
        public Tiny createFromParcel(Parcel source) {
            return new Tiny(source);
        }

        @Override
        public Tiny[] newArray(int size) {
            return new Tiny[size];
        }
    };
}
