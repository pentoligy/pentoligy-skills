
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class VerifiedContributor implements Parcelable {

    @SerializedName("artist")
    private Artist mArtist;
    @SerializedName("contributions")
    private List<String> mContributions;
    @SerializedName("user")
    private User mUser;

    public Artist getArtist() {
        return mArtist;
    }

    public void setArtist(Artist artist) {
        mArtist = artist;
    }

    public List<String> getContributions() {
        return mContributions;
    }

    public void setContributions(List<String> contributions) {
        mContributions = contributions;
    }

    public User getUser() {
        return mUser;
    }

    public void setUser(User user) {
        mUser = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mArtist, flags);
        dest.writeStringList(this.mContributions);
        dest.writeParcelable(this.mUser, flags);
    }

    public VerifiedContributor() {
    }

    protected VerifiedContributor(Parcel in) {
        this.mArtist = in.readParcelable(Artist.class.getClassLoader());
        this.mContributions = in.createStringArrayList();
        this.mUser = in.readParcelable(User.class.getClassLoader());
    }

    public static final Parcelable.Creator<VerifiedContributor> CREATOR = new Parcelable.Creator<VerifiedContributor>() {
        @Override
        public VerifiedContributor createFromParcel(Parcel source) {
            return new VerifiedContributor(source);
        }

        @Override
        public VerifiedContributor[] newArray(int size) {
            return new VerifiedContributor[size];
        }
    };
}
