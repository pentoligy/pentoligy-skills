
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SongRelationship implements Parcelable {

    @SerializedName("songs")
    private List<Song> mSongs;
    @SerializedName("type")
    private String mType;

    public List<Song> getSongs() {
        return mSongs;
    }

    public void setSongs(List<Song> songs) {
        mSongs = songs;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        mType = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.mSongs);
        dest.writeString(this.mType);
    }

    public SongRelationship() {
    }

    protected SongRelationship(Parcel in) {
        this.mSongs = in.createTypedArrayList(Song.CREATOR);
        this.mType = in.readString();
    }

    public static final Parcelable.Creator<SongRelationship> CREATOR = new Parcelable.Creator<SongRelationship>() {
        @Override
        public SongRelationship createFromParcel(Parcel source) {
            return new SongRelationship(source);
        }

        @Override
        public SongRelationship[] newArray(int size) {
            return new SongRelationship[size];
        }
    };
}
