
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Body implements Parcelable {

    @SerializedName("html")
    private String mHtml;

    public String getHtml() {
        return mHtml;
    }

    public void setHtml(String html) {
        mHtml = html;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mHtml);
    }

    public Body() {
    }

    protected Body(Parcel in) {
        this.mHtml = in.readString();
    }

    public static final Parcelable.Creator<Body> CREATOR = new Parcelable.Creator<Body>() {
        @Override
        public Body createFromParcel(Parcel source) {
            return new Body(source);
        }

        @Override
        public Body[] newArray(int size) {
            return new Body[size];
        }
    };
}
