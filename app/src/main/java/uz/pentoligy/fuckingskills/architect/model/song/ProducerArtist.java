
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ProducerArtist implements Parcelable {

    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("header_image_url")
    private String mHeaderImageUrl;
    @SerializedName("id")
    private Long mId;
    @SerializedName("image_url")
    private String mImageUrl;
    @SerializedName("name")
    private String mName;
    @SerializedName("url")
    private String mUrl;

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public String getHeaderImageUrl() {
        return mHeaderImageUrl;
    }

    public void setHeaderImageUrl(String headerImageUrl) {
        mHeaderImageUrl = headerImageUrl;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        mImageUrl = imageUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mApiPath);
        dest.writeString(this.mHeaderImageUrl);
        dest.writeValue(this.mId);
        dest.writeString(this.mImageUrl);
        dest.writeString(this.mName);
        dest.writeString(this.mUrl);
    }

    public ProducerArtist() {
    }

    protected ProducerArtist(Parcel in) {
        this.mApiPath = in.readString();
        this.mHeaderImageUrl = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mImageUrl = in.readString();
        this.mName = in.readString();
        this.mUrl = in.readString();
    }

    public static final Parcelable.Creator<ProducerArtist> CREATOR = new Parcelable.Creator<ProducerArtist>() {
        @Override
        public ProducerArtist createFromParcel(Parcel source) {
            return new ProducerArtist(source);
        }

        @Override
        public ProducerArtist[] newArray(int size) {
            return new ProducerArtist[size];
        }
    };
}
