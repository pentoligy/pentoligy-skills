
package uz.pentoligy.fuckingskills.architect.model.search;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Stats implements Parcelable {

    @SerializedName("unreviewed_annotations")
    private Long mUnreviewedAnnotations;

    public Long getUnreviewedAnnotations() {
        return mUnreviewedAnnotations;
    }

    public void setUnreviewedAnnotations(Long unreviewedAnnotations) {
        mUnreviewedAnnotations = unreviewedAnnotations;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mUnreviewedAnnotations);
    }

    public Stats() {
    }

    protected Stats(Parcel in) {
        this.mUnreviewedAnnotations = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Stats> CREATOR = new Parcelable.Creator<Stats>() {
        @Override
        public Stats createFromParcel(Parcel source) {
            return new Stats(source);
        }

        @Override
        public Stats[] newArray(int size) {
            return new Stats[size];
        }
    };
}
