
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Annotation implements Parcelable {

    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("authors")
    private List<Author> mAuthors;
    @SerializedName("body")
    private Body mBody;
    @SerializedName("comment_count")
    private Long mCommentCount;
    @SerializedName("current_user_metadata")
    private CurrentUserMetadata mCurrentUserMetadata;
    @SerializedName("id")
    private Long mId;
    @SerializedName("share_url")
    private String mShareUrl;
    @SerializedName("state")
    private String mState;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("votes_total")
    private Long mVotesTotal;

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public List<Author> getAuthors() {
        return mAuthors;
    }

    public void setAuthors(List<Author> authors) {
        mAuthors = authors;
    }

    public Body getBody() {
        return mBody;
    }

    public void setBody(Body body) {
        mBody = body;
    }

    public Long getCommentCount() {
        return mCommentCount;
    }

    public void setCommentCount(Long commentCount) {
        mCommentCount = commentCount;
    }

    public CurrentUserMetadata getCurrentUserMetadata() {
        return mCurrentUserMetadata;
    }

    public void setCurrentUserMetadata(CurrentUserMetadata currentUserMetadata) {
        mCurrentUserMetadata = currentUserMetadata;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getShareUrl() {
        return mShareUrl;
    }

    public void setShareUrl(String shareUrl) {
        mShareUrl = shareUrl;
    }

    public String getState() {
        return mState;
    }

    public void setState(String state) {
        mState = state;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public Long getVotesTotal() {
        return mVotesTotal;
    }

    public void setVotesTotal(Long votesTotal) {
        mVotesTotal = votesTotal;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mApiPath);
        dest.writeList(this.mAuthors);
        dest.writeParcelable(this.mBody, flags);
        dest.writeValue(this.mCommentCount);
        dest.writeParcelable(this.mCurrentUserMetadata, flags);
        dest.writeValue(this.mId);
        dest.writeString(this.mShareUrl);
        dest.writeString(this.mState);
        dest.writeString(this.mUrl);
        dest.writeValue(this.mVotesTotal);
    }

    public Annotation() {
    }

    protected Annotation(Parcel in) {
        this.mApiPath = in.readString();
        this.mAuthors = new ArrayList<Author>();
        in.readList(this.mAuthors, Author.class.getClassLoader());
        this.mBody = in.readParcelable(Body.class.getClassLoader());
        this.mCommentCount = (Long) in.readValue(Long.class.getClassLoader());
        this.mCurrentUserMetadata = in.readParcelable(CurrentUserMetadata.class.getClassLoader());
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mShareUrl = in.readString();
        this.mState = in.readString();
        this.mUrl = in.readString();
        this.mVotesTotal = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<Annotation> CREATOR = new Parcelable.Creator<Annotation>() {
        @Override
        public Annotation createFromParcel(Parcel source) {
            return new Annotation(source);
        }

        @Override
        public Annotation[] newArray(int size) {
            return new Annotation[size];
        }
    };
}
