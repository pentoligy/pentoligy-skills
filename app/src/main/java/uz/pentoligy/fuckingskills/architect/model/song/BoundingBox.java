
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class BoundingBox implements Parcelable {

    @SerializedName("height")
    private Long mHeight;
    @SerializedName("width")
    private Long mWidth;

    public Long getHeight() {
        return mHeight;
    }

    public void setHeight(Long height) {
        mHeight = height;
    }

    public Long getWidth() {
        return mWidth;
    }

    public void setWidth(Long width) {
        mWidth = width;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mHeight);
        dest.writeValue(this.mWidth);
    }

    public BoundingBox() {
    }

    protected BoundingBox(Parcel in) {
        this.mHeight = (Long) in.readValue(Long.class.getClassLoader());
        this.mWidth = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<BoundingBox> CREATOR = new Parcelable.Creator<BoundingBox>() {
        @Override
        public BoundingBox createFromParcel(Parcel source) {
            return new BoundingBox(source);
        }

        @Override
        public BoundingBox[] newArray(int size) {
            return new BoundingBox[size];
        }
    };
}
