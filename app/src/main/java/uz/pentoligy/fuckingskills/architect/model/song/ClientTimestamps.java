
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ClientTimestamps implements Parcelable {

    @SerializedName("lyrics_updated_at")
    private Long mLyricsUpdatedAt;
    @SerializedName("updated_by_human_at")
    private Long mUpdatedByHumanAt;

    public Long getLyricsUpdatedAt() {
        return mLyricsUpdatedAt;
    }

    public void setLyricsUpdatedAt(Long lyricsUpdatedAt) {
        mLyricsUpdatedAt = lyricsUpdatedAt;
    }

    public Long getUpdatedByHumanAt() {
        return mUpdatedByHumanAt;
    }

    public void setUpdatedByHumanAt(Long updatedByHumanAt) {
        mUpdatedByHumanAt = updatedByHumanAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.mLyricsUpdatedAt);
        dest.writeValue(this.mUpdatedByHumanAt);
    }

    public ClientTimestamps() {
    }

    protected ClientTimestamps(Parcel in) {
        this.mLyricsUpdatedAt = (Long) in.readValue(Long.class.getClassLoader());
        this.mUpdatedByHumanAt = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<ClientTimestamps> CREATOR = new Parcelable.Creator<ClientTimestamps>() {
        @Override
        public ClientTimestamps createFromParcel(Parcel source) {
            return new ClientTimestamps(source);
        }

        @Override
        public ClientTimestamps[] newArray(int size) {
            return new ClientTimestamps[size];
        }
    };
}
