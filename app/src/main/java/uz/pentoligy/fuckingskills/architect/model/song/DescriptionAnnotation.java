
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;

public class DescriptionAnnotation implements Parcelable {

    @SerializedName("annotatable")
    private Annotatable mAnnotatable;
    @SerializedName("annotations")
    private List<Annotation> mAnnotations;
    @SerializedName("annotator_id")
    private Long mAnnotatorId;
    @SerializedName("annotator_login")
    private String mAnnotatorLogin;
    @SerializedName("api_path")
    private String mApiPath;
    @SerializedName("classification")
    private String mClassification;
    @SerializedName("fragment")
    private String mFragment;
    @SerializedName("id")
    private Long mId;
    @SerializedName("path")
    private String mPath;
    @SerializedName("range")
    private Range mRange;
    @SerializedName("song_id")
    private Long mSongId;
    @SerializedName("url")
    private String mUrl;
    @SerializedName("verified_annotator_ids")
    private List<Object> mVerifiedAnnotatorIds;
    @SerializedName("_type")
    private String m_type;

    public Annotatable getAnnotatable() {
        return mAnnotatable;
    }

    public void setAnnotatable(Annotatable annotatable) {
        mAnnotatable = annotatable;
    }

    public List<Annotation> getAnnotations() {
        return mAnnotations;
    }

    public void setAnnotations(List<Annotation> annotations) {
        mAnnotations = annotations;
    }

    public Long getAnnotatorId() {
        return mAnnotatorId;
    }

    public void setAnnotatorId(Long annotatorId) {
        mAnnotatorId = annotatorId;
    }

    public String getAnnotatorLogin() {
        return mAnnotatorLogin;
    }

    public void setAnnotatorLogin(String annotatorLogin) {
        mAnnotatorLogin = annotatorLogin;
    }

    public String getApiPath() {
        return mApiPath;
    }

    public void setApiPath(String apiPath) {
        mApiPath = apiPath;
    }

    public String getClassification() {
        return mClassification;
    }

    public void setClassification(String classification) {
        mClassification = classification;
    }

    public String getFragment() {
        return mFragment;
    }

    public void setFragment(String fragment) {
        mFragment = fragment;
    }

    public Long getId() {
        return mId;
    }

    public void setId(Long id) {
        mId = id;
    }

    public String getPath() {
        return mPath;
    }

    public void setPath(String path) {
        mPath = path;
    }

    public Range getRange() {
        return mRange;
    }

    public void setRange(Range range) {
        mRange = range;
    }

    public Long getSongId() {
        return mSongId;
    }

    public void setSongId(Long songId) {
        mSongId = songId;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }

    public List<Object> getVerifiedAnnotatorIds() {
        return mVerifiedAnnotatorIds;
    }

    public void setVerifiedAnnotatorIds(List<Object> verifiedAnnotatorIds) {
        mVerifiedAnnotatorIds = verifiedAnnotatorIds;
    }

    public String get_type() {
        return m_type;
    }

    public void set_type(String _type) {
        m_type = _type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mAnnotatable, flags);
        dest.writeTypedList(this.mAnnotations);
        dest.writeValue(this.mAnnotatorId);
        dest.writeString(this.mAnnotatorLogin);
        dest.writeString(this.mApiPath);
        dest.writeString(this.mClassification);
        dest.writeString(this.mFragment);
        dest.writeValue(this.mId);
        dest.writeString(this.mPath);
        dest.writeParcelable(this.mRange, flags);
        dest.writeValue(this.mSongId);
        dest.writeString(this.mUrl);
        dest.writeList(this.mVerifiedAnnotatorIds);
        dest.writeString(this.m_type);
    }

    public DescriptionAnnotation() {
    }

    protected DescriptionAnnotation(Parcel in) {
        this.mAnnotatable = in.readParcelable(Annotatable.class.getClassLoader());
        this.mAnnotations = in.createTypedArrayList(Annotation.CREATOR);
        this.mAnnotatorId = (Long) in.readValue(Long.class.getClassLoader());
        this.mAnnotatorLogin = in.readString();
        this.mApiPath = in.readString();
        this.mClassification = in.readString();
        this.mFragment = in.readString();
        this.mId = (Long) in.readValue(Long.class.getClassLoader());
        this.mPath = in.readString();
        this.mRange = in.readParcelable(Range.class.getClassLoader());
        this.mSongId = (Long) in.readValue(Long.class.getClassLoader());
        this.mUrl = in.readString();
        this.mVerifiedAnnotatorIds = new ArrayList<Object>();
        in.readList(this.mVerifiedAnnotatorIds, Object.class.getClassLoader());
        this.m_type = in.readString();
    }

    public static final Parcelable.Creator<DescriptionAnnotation> CREATOR = new Parcelable.Creator<DescriptionAnnotation>() {
        @Override
        public DescriptionAnnotation createFromParcel(Parcel source) {
            return new DescriptionAnnotation(source);
        }

        @Override
        public DescriptionAnnotation[] newArray(int size) {
            return new DescriptionAnnotation[size];
        }
    };
}
