package uz.pentoligy.fuckingskills.architect.model.landinginfo;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

/**
 * Created by pentoligy on 4/26/17.
 */

public class LandingInfoItem {
    private int iconDrawableId, titleStringId, textStringId;

    public LandingInfoItem addItem(@DrawableRes int iconDrawableId, @StringRes int titleStringId,
                                   @StringRes int textStringId) {
        LandingInfoItem item = new LandingInfoItem();
        item.setIconDrawableId(iconDrawableId);
        item.setTitleStringId(titleStringId);
        item.setTextStringId(textStringId);
        return item;
    }

    public int getIconDrawableId() {
        return iconDrawableId;
    }

    private void setIconDrawableId(int iconDrawableId) {
        this.iconDrawableId = iconDrawableId;
    }

    public int getTitleStringId() {
        return titleStringId;
    }

    private void setTitleStringId(int titleStringId) {
        this.titleStringId = titleStringId;
    }

    public int getTextStringId() {
        return textStringId;
    }

    private void setTextStringId(int textStringId) {
        this.textStringId = textStringId;
    }
}
