
package uz.pentoligy.fuckingskills.architect.model.song;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Accept implements Parcelable {

    @SerializedName("primary")
    private Primary mPrimary;

    public Primary getPrimary() {
        return mPrimary;
    }

    public void setPrimary(Primary primary) {
        mPrimary = primary;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.mPrimary, flags);
    }

    public Accept() {
    }

    protected Accept(Parcel in) {
        this.mPrimary = in.readParcelable(Primary.class.getClassLoader());
    }

    public static final Parcelable.Creator<Accept> CREATOR = new Parcelable.Creator<Accept>() {
        @Override
        public Accept createFromParcel(Parcel source) {
            return new Accept(source);
        }

        @Override
        public Accept[] newArray(int size) {
            return new Accept[size];
        }
    };
}
