package uz.pentoligy.fuckingskills.ui.fragment;

import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.core.ui.base.BaseFragment;
import uz.pentoligy.fuckingskills.dagger2.component.FragmentComponent;
import uz.pentoligy.fuckingskills.recyclerview.adapter.LandingInfoAdapter;

public class MainFragment extends BaseFragment implements View.OnClickListener {
    private static final int EASTER_EGG_CLICKS_COUNT = 16;
    private int currentClicksCount = 0;

    private RecyclerView recyclerView;

    @Override
    public int getFragmentLayout() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle parentInstanceState, @Nullable Bundle childInstanceState) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getMainActivity());
        LandingInfoAdapter landingInfoAdapter = new LandingInfoAdapter(getMainActivity(), this);

        recyclerView = (RecyclerView) view.findViewById(R.id.fragment_recycler_view);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(landingInfoAdapter);
        recyclerView.setItemViewCacheSize(landingInfoAdapter.getItemCount());
    }

    @Override
    public void onPauseSavedInstanceState(Bundle outState) {

    }

    @Override
    public void inject(FragmentComponent fragmentComponent) {
        fragmentComponent.inject(this);
    }

    @Override
    public void onClick(View view) {
        if (currentClicksCount != -1) {
            if (++currentClicksCount == EASTER_EGG_CLICKS_COUNT) {
                currentClicksCount = -1;
                ((LandingInfoAdapter) recyclerView.getAdapter()).addUGTEasterEgg();
                Snackbar.make(recyclerView, getString(R.string.snackbar_easter_egg_message), Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
