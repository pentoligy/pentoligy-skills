package uz.pentoligy.fuckingskills.ui.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import javax.inject.Inject;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;
import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.architect.model.song.Song;
import uz.pentoligy.fuckingskills.architect.presenter.GeneralPresenter;
import uz.pentoligy.fuckingskills.architect.presenter.activity.PopupActivityPresenter;
import uz.pentoligy.fuckingskills.core.animation.ViewAnimation;
import uz.pentoligy.fuckingskills.core.glide.palette.GlidePaletteAsyncListener;
import uz.pentoligy.fuckingskills.core.glide.palette.GlidePaletteBuilder;
import uz.pentoligy.fuckingskills.core.ui.presenter.BaseActivityPresenterActivity;
import uz.pentoligy.fuckingskills.dagger2.component.AppCompatActivityComponent;
import uz.pentoligy.fuckingskills.dagger2.model.Glide;


/**
 * Created by pentoligy on 4/23/17.
 */

public class PopupActivity extends BaseActivityPresenterActivity<PopupActivityPresenter, Song> implements View.OnClickListener, GlidePaletteAsyncListener {
    public final static String NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY = "intent_music_info_string_array";

    @Inject Glide glide;

    private GlidePaletteBuilder glidePaletteBuilder;

    private LinearLayout stateSuccessLinearLayout, stateLinearLayout;
    private RelativeLayout titleRelativeLayout, albumTitleRelativeLayout, writtenByTitleRelativeLayout,
            recordedAtTitleRelativeLayout, releaseDateTitleRelativeLayout;
    private ScrollView scrollView;
    private AppCompatImageView backgroundImageView, geniusLogoImageView, albumArtImageView;
    private AppCompatImageButton shareImageButton;
    private AppCompatTextView titleTextView, albumTitleHolderTextView,
            albumTitleTextView, writtenByHolderTextView, writtenByTextView,
            recordedAtHolderTextView, recordedAtTextView, releaseDateHolderTextView, releaseDateTextView,
            aboutSongTextView, retryTitleTextView;
    private AppCompatButton closeButton, retryButton;

    private MaterialProgressBar loadingProgressBar;

    private String artist, album, song;

    @Override
    public PopupActivityPresenter setPresenter() {
        return new PopupActivityPresenter();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup);
        setFinishOnTouchOutside(false);

        stateSuccessLinearLayout = (LinearLayout) findViewById(R.id.activity_popup_state_success_relative_layout);
        stateLinearLayout = (LinearLayout) findViewById(R.id.activity_popup_state_linear_layout);

        titleRelativeLayout = (RelativeLayout) findViewById(R.id.activity_popup_title_relative_layout);
        albumTitleRelativeLayout = (RelativeLayout) findViewById(R.id.activity_popup_album_title_relative_layout);
        writtenByTitleRelativeLayout = (RelativeLayout) findViewById(R.id.activity_popup_written_by_title_relative_layout);
        recordedAtTitleRelativeLayout = (RelativeLayout) findViewById(R.id.activity_popup_recorded_at_title_relative_layout);
        releaseDateTitleRelativeLayout = (RelativeLayout) findViewById(R.id.activity_popup_release_date_title_relative_layout);

        scrollView = (ScrollView) findViewById(R.id.activity_popup_about_song_scroll_view);

        backgroundImageView = (AppCompatImageView) findViewById(R.id.activity_popup_background_image_view);
        geniusLogoImageView = (AppCompatImageView) findViewById(R.id.activity_popup_genius_logo_image_view);
        albumArtImageView = (AppCompatImageView) findViewById(R.id.activity_popup_album_art_image_view);

        shareImageButton = (AppCompatImageButton) findViewById(R.id.activity_popup_share_button);

        titleTextView = (AppCompatTextView) findViewById(R.id.activity_popup_title_text_view);
        albumTitleHolderTextView = (AppCompatTextView) findViewById(R.id.activity_popup_album_title_holder_text_view);
        albumTitleTextView = (AppCompatTextView) findViewById(R.id.activity_popup_album_title_text_view);
        writtenByHolderTextView = (AppCompatTextView) findViewById(R.id.activity_popup_written_by_title_holder_text_view);
        writtenByTextView = (AppCompatTextView) findViewById(R.id.activity_popup_written_by_title_text_view);
        recordedAtHolderTextView = (AppCompatTextView) findViewById(R.id.activity_popup_recorded_at_title_holder_text_view);
        recordedAtTextView = (AppCompatTextView) findViewById(R.id.activity_popup_recorded_at_title_text_view);
        releaseDateHolderTextView = (AppCompatTextView) findViewById(R.id.activity_popup_release_date_title_holder_text_view);
        releaseDateTextView = (AppCompatTextView) findViewById(R.id.activity_popup_release_date_title_text_view);
        aboutSongTextView = (AppCompatTextView) findViewById(R.id.activity_popup_about_song_text_view);
        retryTitleTextView = (AppCompatTextView) findViewById(R.id.activity_popup_retry_title_text_view);

        closeButton = (AppCompatButton) findViewById(R.id.activity_popup_button);
        retryButton = (AppCompatButton) findViewById(R.id.activity_popup_retry_button);

        loadingProgressBar = (MaterialProgressBar) findViewById(R.id.activity_popup_loading_material_progress_bar);

        bindView();

        shareImageButton.setOnClickListener(this);
        retryButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);

        geniusLogoImageView.setColorFilter(ContextCompat.getColor(this, R.color.colorAccent));

        if (getIntent().getStringArrayExtra(NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY) != null) {
            artist = getIntent().getStringArrayExtra(NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY)[0];
            album = getIntent().getStringArrayExtra(NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY)[1];
            song = getIntent().getStringArrayExtra(NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY)[2];

            titleTextView.setText(song + getString(R.string.by_holder) + artist);

            if (savedInstanceState == null) {
                onOpenedFirst();
            } else {
                if (getPresenter().getModel() != null) {
                    onSuccess(getPresenter().getModel());
                }
            }

        } else {
            finish();
        }
    }

    @Override
    public void inject(AppCompatActivityComponent appCompatActivityComponent) {
        appCompatActivityComponent.inject(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        startActivity(songInfoIntent(intent.getStringArrayExtra(PopupActivity.NOTIFICATION_INTENT_MUSIC_INFO_STRING_ARRAY)));
        finish();
    }

    @Override
    public void onClick(View view) {
        if (view == closeButton) {
            finish();
        } else if (view == shareImageButton) {
            startActivity(shareUrlIntent(titleTextView.getText().toString(),
                    getPresenter().getModel().getUrl()));
        } else if (view == retryButton) {
            getPresenter().getItems(artist, song);
        }
    }

    @Override
    public void onOpenedFirst() {
        getPresenter().getItems(artist, song);
        retryButton.setVisibility(View.INVISIBLE);
    }


    @Override
    public void onLoading() {
        stateSuccessLinearLayout.setVisibility(View.INVISIBLE);
        stateLinearLayout.setVisibility(View.VISIBLE);

        ViewAnimation.fadeIn(false, stateLinearLayout);

        titleRelativeLayout.setVisibility(View.INVISIBLE);
        loadingProgressBar.setVisibility(View.VISIBLE);
        retryTitleTextView.setVisibility(View.INVISIBLE);
        retryButton.setText(getString(R.string.waiting));
        retryButton.setEnabled(false);
    }

    @Override
    public void onSuccess(Song item) {

        glidePaletteBuilder = glide.getPaletteBuilder(this, backgroundImageView);

        glide.load(item.getSongArtUrl())
                .asBitmap()
                .transform(glide.getBlurTransformation(),
                        glide.getColorFilterTransformation(ContextCompat.getColor(this, R.color.colorTintAlbumArt)))
                .into(glidePaletteBuilder);

        glide.load(item.getAlbum().getCoverArtUrl()).into(albumArtImageView);

        if (item.getAlbum().getName() == null) {
            albumTitleRelativeLayout.setVisibility(View.GONE);
        } else {
            albumTitleTextView.setText(item.getAlbum().getName());
        }

        if (item.getWriters() == null | TextUtils.isEmpty(item.getWriters())) {
            writtenByTitleRelativeLayout.setVisibility(View.GONE);
        } else {
            writtenByTextView.setText(item.getWriters());
        }

        if (item.getRecordingLocation() == null | TextUtils.isEmpty(item.getRecordingLocation())) {
            recordedAtTitleRelativeLayout.setVisibility(View.GONE);
        } else {
            recordedAtTextView.setText(item.getRecordingLocation());
        }

        if (item.getReleaseDate() == null) {
            releaseDateTitleRelativeLayout.setVisibility(View.GONE);
        } else {
            releaseDateTextView.setText(item.getReleaseDateInWords());
        }

        if (item.getDescription().getHtml().equals("<p>?</p>")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                aboutSongTextView.setText(Html.fromHtml(getString(R.string.no_description_provided).trim(), Html.FROM_HTML_MODE_LEGACY));
            } else {
                aboutSongTextView.setText(Html.fromHtml(getString(R.string.no_description_provided).trim()));
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                aboutSongTextView.setText(Html.fromHtml(item.getDescription().getHtml().trim(),
                        glide.getImageGetter(aboutSongTextView), glide.getTagHandler()));
            } else {
                aboutSongTextView.setText(Html.fromHtml(item.getDescription().getHtml().trim(),
                        glide.getImageGetter(aboutSongTextView), glide.getTagHandler()));
            }
        }

        aboutSongTextView.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onGenerated(int titleTextColor, int bodyTextColor) {
        shareImageButton.getDrawable().setColorFilter(titleTextColor, PorterDuff.Mode.SRC_IN);
        titleTextView.setTextColor(titleTextColor);
        albumTitleHolderTextView.setTextColor(titleTextColor);
        albumTitleTextView.setTextColor(titleTextColor);
        writtenByTextView.setTextColor(titleTextColor);
        writtenByHolderTextView.setTextColor(titleTextColor);
        recordedAtTextView.setTextColor(titleTextColor);
        recordedAtHolderTextView.setTextColor(titleTextColor);
        releaseDateTextView.setTextColor(titleTextColor);
        releaseDateHolderTextView.setTextColor(titleTextColor);
        aboutSongTextView.setTextColor(titleTextColor);
        aboutSongTextView.setLinkTextColor(ContextCompat.getColor(this, R.color.colorAccent));

        ViewAnimation.fadeOut(false, stateLinearLayout);
        ViewAnimation.fadeIn(false, titleRelativeLayout);
        ViewAnimation.fadeIn(false, stateSuccessLinearLayout);

        ViewAnimation.fromColorToColorRepeated(ContextCompat.getColor(this, R.color.colorAccent),
                bodyTextColor, geniusLogoImageView);
    }

    @Override
    public void onError(String throwableMessage) {
        stateSuccessLinearLayout.setVisibility(View.INVISIBLE);
        stateLinearLayout.setVisibility(View.VISIBLE);
        loadingProgressBar.setVisibility(View.INVISIBLE);
        retryTitleTextView.setVisibility(View.VISIBLE);
        retryButton.setVisibility(!throwableMessage.equals(GeneralPresenter.ERROR_LYRICS_NOT_FOUND) ? View.VISIBLE : View.GONE);

        retryTitleTextView.setText(getString(!throwableMessage.equals(
                GeneralPresenter.ERROR_LYRICS_NOT_FOUND) ? R.string.connection_error : R.string.lyrics_not_found));

        retryButton.setEnabled(!throwableMessage.equals(GeneralPresenter.ERROR_LYRICS_NOT_FOUND));
        retryButton.setText(getString(R.string.retry));
    }
}
