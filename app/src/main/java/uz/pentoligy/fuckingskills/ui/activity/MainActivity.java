package uz.pentoligy.fuckingskills.ui.activity;

import android.os.Bundle;

import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.core.ui.base.BaseAppCompatActivity;
import uz.pentoligy.fuckingskills.dagger2.component.AppCompatActivityComponent;
import uz.pentoligy.fuckingskills.widget.Toolbar;

public class MainActivity extends BaseAppCompatActivity {
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    @Override
    public void inject(AppCompatActivityComponent appCompatActivityComponent) {
        appCompatActivityComponent.inject(this);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }
}
