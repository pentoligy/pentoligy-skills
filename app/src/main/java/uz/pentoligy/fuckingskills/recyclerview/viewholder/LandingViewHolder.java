package uz.pentoligy.fuckingskills.recyclerview.viewholder;

import android.view.View;

import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.architect.model.landinginfo.LandingInfoItem;
import uz.pentoligy.fuckingskills.core.ui.dagger2.DaggerViewHolder;
import uz.pentoligy.fuckingskills.dagger2.component.ViewHolderComponent;
import uz.pentoligy.fuckingskills.widget.LandingInfo;

/**
 * Created by pentoligy on 4/26/17.
 */

public class LandingViewHolder extends DaggerViewHolder {
    private View.OnClickListener onClickListener;
    private LandingInfo landingInfo;

    public LandingViewHolder(View.OnClickListener onClickListener, View itemView) {
        super(itemView);
        this.onClickListener = onClickListener;
        landingInfo = (LandingInfo) itemView.findViewById(R.id.viewholder_landing_info_item);
    }

    public void initItemView(LandingInfoItem item) {
        landingInfo.setOnClickListener(onClickListener);
        landingInfo.setIcon(item.getIconDrawableId());
        landingInfo.setTitle(item.getTitleStringId());
        landingInfo.setText(item.getTextStringId());
    }

    @Override
    public void inject(ViewHolderComponent viewHolderComponent) {
        viewHolderComponent.inject(this);
    }
}
