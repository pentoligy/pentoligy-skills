package uz.pentoligy.fuckingskills.recyclerview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import uz.pentoligy.fuckingskills.R;
import uz.pentoligy.fuckingskills.architect.model.landinginfo.LandingInfoItem;
import uz.pentoligy.fuckingskills.recyclerview.viewholder.LandingViewHolder;

/**
 * Created by pentoligy on 4/26/17.
 */

public class LandingInfoAdapter extends RecyclerView.Adapter<LandingViewHolder> {
    private ArrayList<LandingInfoItem> landingInfoItems;
    private View.OnClickListener onClickListener;
    private Context context;

    public LandingInfoAdapter(Context context, View.OnClickListener onClickListener) {
        this.landingInfoItems = getLandingInfoItems();
        this.context = context;
        this.onClickListener = onClickListener;
    }

    @Override
    public LandingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new LandingViewHolder(onClickListener,
                LayoutInflater.from(context).inflate(R.layout.utils_recyclerview_viewholder_landing_info_item, parent, false));
    }

    @Override
    public void onBindViewHolder(LandingViewHolder holder, int position) {
        holder.initItemView(landingInfoItems.get(position));
    }

    @Override
    public int getItemCount() {
        return landingInfoItems.size();
    }

    public void addUGTEasterEgg() {
        landingInfoItems.add(getItemCount() - 1, new LandingInfoItem().addItem(R.drawable.icon, R.string.landing_title_4, R.string.landing_text_4));
        notifyDataSetChanged();
    }

    private ArrayList<LandingInfoItem> getLandingInfoItems() {
        ArrayList<LandingInfoItem> items = new ArrayList<>();
        items.add(new LandingInfoItem().addItem(R.drawable.ic_hi, R.string.landing_title_1, R.string.landing_text_1));
        items.add(new LandingInfoItem().addItem(R.drawable.ic_pentoligy, R.string.landing_title_2, R.string.landing_text_2));
        items.add(new LandingInfoItem().addItem(R.drawable.ic_work, R.string.landing_title_3, R.string.landing_text_3));
        items.add(new LandingInfoItem().addItem(R.mipmap.ic_launcher, R.string.landing_title_5, R.string.landing_text_5));
        return items;
    }
}
