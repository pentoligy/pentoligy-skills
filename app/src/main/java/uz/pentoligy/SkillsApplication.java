package uz.pentoligy;

import android.app.Application;

import uz.pentoligy.fuckingskills.dagger2.component.ApplicationComponent;
import uz.pentoligy.fuckingskills.dagger2.component.DaggerApplicationComponent;
import uz.pentoligy.fuckingskills.dagger2.module.ApplicationModule;
import uz.pentoligy.fuckingskills.dagger2.module.GlideModule;
import uz.pentoligy.fuckingskills.dagger2.module.NetworkModule;
import uz.pentoligy.fuckingskills.dagger2.module.ServiceManagersModule;


/**
 * Created by pentoligy on 4/22/17.
 */

public class SkillsApplication extends Application {
    private ApplicationComponent applicationComponent;
    private static SkillsApplication applicationInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        applicationInstance = this;

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .serviceManagersModule(new ServiceManagersModule())
                .networkModule(new NetworkModule())
                .glideModule(new GlideModule())
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static SkillsApplication getInstance() {
        return applicationInstance;
    }

}
